
#A Survey on Survey of Migration of Legacy Systems
##Citing
ArticleAuthorRef: (99659083629@ACM;a ZoaArticleReference)16

##Authors
Auhor Name: Ganesan, A. Sivagnana
Auhor ID: 99659083629@ACM; 
Auhor Affiliations:  Dept. of Banking Technology, Pondicherry University, Pondicherry, India; 
Auhor Name: Chithralekha, T.
Auhor ID: 81553755756@ACM; 
Auhor Affiliations:  Dept. of Computer Science & Engg., Pondicherry University, Pondicherry, India; 

##Abstract
Legacy systems are mission critical complex systems which are hard to maintain owing to shortage of skill sets and monolithic code architecture with tightly coupled tiers, all of which are indication of obsoleteness of the system technology. Thus, they have to be migrated to the latest technology(ies). Migration is an offspring research in Software Engineering which is almost three decades old research and numerous publications have emerged in many topics in the migration domain with focus areas of code migration, architecture migration, case study on migration and effort estimation on migration. In addition, various survey works surveying different aspects of migration have also emerged. This paper provides a survey of these survey works in order to provide a consolidation of these survey works. As an outcome of the survey on survey of migration, a road map of migration evolution from the early stages to the recent works on migration comprising of significant milestones in migration research has been presented in this work.
##Keywords
Migration;Legacy Systems;Software Evolution;SOA
##Overview
The artile surveys many other surveys and particular articles. It finishes with some over all analysis.##Background on migration	Some definitions	Migration is the passage of a current operating environment to an other.	Migration is to move to a new technical environment.	Migration appears in different levels: language migration, operative system, UI, architecture. 	Migration comes in different flavors too: automatic, semi-automatic and manual.	##Phases of migration	__Legacy system understanding__	__Target system understanding__	__Migration feasability analysis__	__Target system development__	__Deployment & provisioning of target system__##Earlier Migration approachesNota: The earlier articles talk about approaches to tackle down such problem-Gateway based 	* Database first	* Database last	* Composite database 	* Chicken little- Non gateway	* Big-Bang	* Butterfly##SOA Migration ApproachesNota: This approaches are more tending to software reuse more than real migration., based on tooling and framework.  	* SMART  (tool suite - Service Migration and Reuse Technique)	* MASHUP (tool suite- MigrAtion to Service Harmonization computing platform technology) 	* Sneed (approach - Sneed is the author name)	* SOMA (Service Oriented Modeling and Analysis) 	* Two View Approach 	## Cloud Migration		too light. 	 	

##References
###This article is cited by
 Using programming by demonstration for multimodality in mobile-human interactions (10.1145/3132129.3132154)
 Incremental Construction for Scalable Component-Based Systems (https://www.mdpi.com/1424-8220/20/5/1435)
 Cloud Readiness as an Enabler for Application Rationalization: A Survey in the Netherlands (http://link.springer.com/10.1007/978-3-319-70305-3_7)
 Proceedings of the 10th International Conference on Computer Modeling and Simulation (10.1145/3177457.3177478)
 Journal of Advanced Computational Intelligence and Intelligent Informatics (https://www.fujipress.jp/jaciii/jc/jacii002200030295)
 2017 11th International Conference on Software, Knowledge, Information Management and Applications (SKIMA). (http://ieeexplore.ieee.org/document/82941
 2019 6th International Conference on Research and Innovation in Information Systems (ICRIIS). (https://ieeexplore.ieee.org/document/9073628/)
###This article Cites
 REMICS: http://www.remics.eu/home. Acces
 Six Steps to Migration Project Success: 
 Ransom, J., Warren, I., Sommerville, I.,
 Malinova, A. 2010. Approaches and Techni
 Razavian, M. and Lago, P. 2015. A system
 Bisbal, J., Lawless, D., Wu, B., Grimson
 Girish, P.S. and Guruprasad, H.S. 2014. 
 Jamshidi, P., Ahmad, A. and Pahl, C. 201
 Gimnich, R. and Winter, A. 2007. SOA Mig
 Five Ways to Migrate Applications to the
 Sneed, H.M. and Gmbh, A. 2006. Integrati
 Balasubramaniam, S., Lewis, G.A., Morris
 Rodríguez, A., Caro, A. and Fernández-Me
 Beserra, P. V., Camara, A., Ximenes, R.,
 Ali, S. and Abdelhak-Djamel, S. 2012. Ev
 Khadka, R., Saeidi, A., Idu, A., Hage, J
 Winter, a., Zillmann, C., Herget, A., Te
 Cetin, S., Altintas, N.I., Oguztuzun, H.
 Khadka, R., Saeidi, A., Jansen, S. and H
 Tilley, S. 1998. A Reverse- Engineering 
 Comella-Dorda, S., Wallnau, K., Seacord,
 Smith, D. 2007. Migration of legacy asse
 Wan, Z. and Wang, P. 2014. A Survey and 
 Comella-dorda, S., Wallnau, K., Seacord,
 Khadka, R., Saeidi, A., Idu, A., Hage, J
 Wafa, S. 2013. Migration of Legacy Syste
 Rasool, G. and Asif, N. 2007. Software A

##Article Metadata
NumberOfPages 10
pages an OrderedCollection(1)
EventType PAPER_CONFERENCE
CollectionTitle ICIA-16
CollectionNumber 72
PublisherCity New York, NY, USA
Number 72
isbn 9781450347563
EventCity Pondicherry, India
