
#Quality driven software migration of procedural code to object-oriented design
##Citing
ArticleAuthorRef: (37275730500@IEEE;a ZoaArticleReference)5

##Authors
Auhor Name: Ying Zou, Ying Zou
Auhor ID: 37275730500@IEEE; 
Auhor Affiliations:  

##Abstract
In the context of software maintenance, legacy software systems are continuously re-engineered in order to correct errors, provide new functionality, or port them into modern platforms. However, software re-engineering activities should not occur in a vacuum, and it is important to incorporate non-functional requirements as part of the re-engineering process. We present an incremental reengineering framework that allows for quality requirements to be modeled as soft-goals, and transformations to be applied selectively towards achieving specific quality requirements for the target system. To deal with large software systems, we decompose the system into a collection of smaller clusters. The reengineering framework can be applied incrementally to each of these clusters and results are assembled to produce the final system.
##Keywords
Assembly systems;Application software;legacy software systems re-engineering;Mission critical systems;quality driven software migration;Software maintenance;software maintenance;procedural code;software quality;systems re-engineering;Software systems;object-oriented methods;Software quality;object-oriented design;Computer errors;Information analysis;object-oriented programming;incremental reengineering framework;Error correction;Object oriented modeling
##Overview
The paper sumarizes the thesis of the author. It means in general terms the contribution of the thesis is:- Modeling quality requirements. Typically defined top-down: Functionality, - Model and apply unified rules to transform the code from procedural to object oriented.- A quality driven approach (based on finding, through the successive application of the previously defined rules, we find the models that better respond to the expected qualities) - Incremental Process: the process is thought to be applied incrementally working at the level of clusters. - Quality driven evaluation techniques	- generate possible outcomes	- compare by quality metrics 

##References
###This article is cited by
 Mario Bernhart, Andreas Mauczka, Michael Fiedler, Stefan Strobl, Thomas Grechenig, "Incremental reengineering and migration of a 40 year old airport o
###This article Cites
 M.M. Lehman, "Programs, life cycles, and
 Paul Alphen and Dick Bergem. Markov mode
 K. Kontogiannis, P. Patil, "Evidence Dri
 S. Mancoridis, B.S. Mitchell, Y. Chen, a
 C. Lindig and G. Snelting, "Assessing Mo
 H. A. Sahraoui, W. Melo, H. Lounis, F. D
 Lionel C. Briand, Jurgen Wst, and Hakim 
 John Mylopoulos et. al., "Representing a
 De Lucia, G.A. Di Lucca, A.R. Fasolino, 
 Letha H. Etzkorn, Carl G. Davis, "Automa
 Filippo Lanubile, and Giuseppe Visaggio,

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=10097
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'1510179' 'type'->'PDF' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 26-29 Sept. 2005
pdfUrl nil
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
isNumber 32336
isPromo false
startPage 709
isEphemera false
isACM false
mlTime PT0.032268S
isSAE false
isMorganClaypool false
sourcePdf 01510179.pdf
isFreeDocument false
isDynamicHtml false
pdfPath /iel5/10097/32336/01510179.pdf
isMarketingOptIn false
articleId 1510179
isOpenAccess false
isSMPTE false
chronOrPublicationDate 26-29 Sept. 2005
isEarlyAccess false
endPage 713
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1063-6773' ))
mediaPath 
isStaticHtml false
confLoc Budapest, Hungary, Hungary
isGetAddressInfoCaptured false
content_type Conferences
accessionNumber 8774892
html_flag false
pubLink /xpl/conhome/10097/proceeding
lastupdate 2020-08-03
isChapter false
isStandard false
isCustomDenial false
publicationYear 2005
htmlAbstractLink /document/1510179/
issueLink /xpl/tocresult.jsp?isnumber=32336
displayPublicationTitle 21st IEEE International Conference on Software Maintenance (ICSM'05)
isGetArticle false
ml_html_flag false
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'false' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
openAccessFlag F
isJournal false
publicationNumber 10097
dbTime 3 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
chronDate 26-29 Sept. 2005
xploreDocumentType Conference Publication
isProduct false
citationCount 1
metrics a Dictionary('citationCountPaper'->1 'citationCountPatent'->2 'totalDownloads'->103 )
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle Quality driven software migration of procedural code to object-oriented design
articleNumber 1510179
publicationTitle 21st IEEE International Conference on Software Maintenance (ICSM'05)
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-7695-2368-4' ))
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Quality+driven+software+migration+of+procedural+code+to+object-oriented+design&isbn=0-7695-2368-4&publicationDate=2005&author=Y.+Zou&ContentID=10.1109/ICSM.2005.71&orderBeanReset=true&startPage=709&endPage=713&proceedingName=21st+IEEE+International+Conference+on+Software+Maintenance+%28ICSM%2705%29
getProgramTermsAccepted false
isConference true
doiLink https://doi.org/10.1109/ICSM.2005.71
isNotDynamicOrStatic true
isBook false
isOUP false
displayDocTitle Quality driven software migration of procedural code to object-oriented design
