
#Incubating services in legacy systems for architectural migration
##Citing
ArticleAuthorRef: (37279633000@IEEE;a ZoaArticleReference)5

##Authors
Auhor Name: Zhang, Z.
Auhor ID: 37279633000@IEEE; 
Auhor Affiliations:  
Auhor Name: Yang, H.
Auhor ID: 37278138100@IEEE; 
Auhor Affiliations:  

##Abstract
Legacy systems are valuable assets for organisations. They continuously evolve with new emerged technologies in rapidly changing business environment. Web services, together with service-oriented architectures, facilitate legacy system webification and evolution in service-oriented computing environment. Traditional approaches to integrate legacy systems with Web service technology are wrapping a legacy system as a black-box without adequate system understanding. In this paper, we proposed a reengineering approach which applies an improved agglomerative hierarchical clustering algorithm to restructure legacy code and to facilitate legacy code extraction for Web service construction. It supports service identification and service packaging and archives legacy system migration into service-oriented architectures by providing functional legacy code as Web services.
##Keywords
Clustering algorithms;Software Reengineering;Legacy System;Software systems;Web service;open systems;Software engineering;Web Services;Hierarchical Clustering;Application software;Internet;Logic;service-oriented architecture;Service-Oriented Architectures (SOA);Laboratories;Wrapping;systems re-engineering;legacy system;Business process re-engineering;Service oriented architecture;business environment;hierarchical clustering algorithm;Web services;software architecture;software reengineering;software maintenance;Software Evolution
##Overview
The article explains how with SOA technology the need to discard existing legacy systems disapear, but still hard to make them compatible for the service oriented usage, since most of them have been designed with an other approach in mind.The approach proposes to use a clustering analysis over the loaded model of the origin software to show what are the  possible services by getting an augmented view of the code clusterised by conceptual proprerties. The algorithm is based on naming to calculate similitude. It is also prepared to mix procedural and object oriented source code.After the piece of code is recogniced, it is required to refactorised it up to the moment that the code is lousy and lowly coupled. It is required to sensure that the piece of code does not means undesirable side effects. For service packaging is proposed modify the code to be able to be wrapped and called from Java. Despite the fact that this is an invasive method, the result is evaluated on a real software. The output is a set of C++ developed features running on top of Java SOAP environment, served by an Apache Tomcat (really cheap in comparison with other solutions such as JBoss, etc)

##References
###This article is cited by
 Haniche Fayçal, Mellah Hakima, Drias Habiba, "An Agent Based Encapsulator System: For Integrating and Composing Legacy System Functionalities", (https
 Zhongxin Wu, DePei Qian, YuJie Wang, Yongjian Wang, Dongbo Yang, "An Available Service Intermediator for E-government", (https://scholar.google.com/sc
 PengWei Wang, ZhiJun Ding, ChangJun Jiang, MengChu Zhou, "Design and Implementation of a Web-Service-Based Public-Oriented Personalized Health Care Pl
 J. Matković, K. Fertalj, "Models for the development of Web service orchestrations", (https://scholar.google.com/scholar?as_q=Models+for+the+developme
 Hong Zhou, Hongji Yang, Andrew Hugill, "An Ontology-Based Approach to Reengineering Enterprise Software for Cloud Computing", (https://scholar.google.
 Giusy Di Lorenzo, Anna Rita Fasolino, Lorenzo Melcarne, Porfirio Tramontana, Valeria Vittorini, "Turning Web Applications into Web Services by Wrappin
 Zhuo Zhang, Hongji Yang, Dongdai Zhou, Shaochun Zhong, "A SOA Based Approach to User-Oriented System Migration", (https://scholar.google.com/scholar?a
 Hiroshi Wada, Junichi Suzuki, Katsuya Oba, "A Service-Oriented Design Framework for Secure Network Applications", (https://scholar.google.com/scholar?
 Liang Bao, Chao Yin, Weigang He, Jun Ge, Ping Chen, "Extracting reusable services from legacy object-oriented systems", (https://scholar.google.com/sc
 Ravi Khadka, Amir Saeidi, Slinger Jansen, Jurriaan Hage, "A structured legacy to SOA migration process and its evaluation in practice", (https://schol
 Nianjun Zhou, Liang-Jie Zhang, Yi-Min Chee, Lei Chen, "Legacy Asset Analysis and Integration in Model-Driven SOA Solution", (https://scholar.google.co
 F. Chen, S. Li, H. Yang, Ching-Huey Wang, W. Cheng-Chung Chu, "Feature analysis for service-oriented reengineering", (https://scholar.google.com/schol
 He Guo, Chunyan Guo, Feng Chen, Hongji Yang, "Wrapping Client-Server Application to Web Services for Internet Computing", (https://scholar.google.com/
 Saad Alahmari, Ed Zaluska, David De Roure, "A Service Identification Framework for Legacy System Migration into SOA", (https://scholar.google.com/scho
 Daniel Hallmans, Thomas Nolte, Stig Larsson, "Industrial Requirements on Evolution of an Embedded System Architecture", (https://scholar.google.com/sc
 Encarna Sosa-Sánchez, Pedro J. Clemente, Miguel Sánchez-Cabrera, José M. Conejero, Roberto Rodríguez-Echeverría, Fernando Sánchez-Figueroa, "Service D
 Feng Chen, Shaoyun Li, Hongji Yang, "Enforcing Role-Based Access Controls in Software Systems with an Agent Based Service Oriented Approach", (https:/
 Masahide Nakamura, Akihiro Tanaka, Hiroshi Igaki, Haruaki Tamada, Ken-ichi Matsumoto, "Adapting Legacy Home Appliances to Home Network Systems UsingWe
 Hiroshi Wada, Junichi Suzuki, Katsuya Oba, "Modeling Non-Functional Aspects in Service Oriented Architecture", (https://scholar.google.com/scholar?as_
 Maulahikmah Galinium, Negar Shahbaz, "Success factors model: Case studies in the migration of legacy systems to Service Oriented Architecture", (https
 Saad Alahmari, David de Roure, Ed Zaluska, "A Model-Driven Architecture Approach to the Efficient Identification of Services on Service-Oriented Enter
 Feng Chen, Zhuopeng Zhang, Jianzhi Li, Jian Kang, Hongji Yang, "Service Identification via Ontology Mapping", (https://scholar.google.com/scholar?as_q
 Jia Zhang, Carl K. Chang, Liang-Jie Zhang, Patrick C. K. Hung, "Toward a Service-Oriented Development Through a Case Study", (https://scholar.google.c
 Suman Jain, Inderveer Chana, "Reengineering process of legacy systems for the cloud: An overview", (https://scholar.google.com/scholar?as_q=Reengineer
 Richard Millham, "Migration of a Legacy Procedural System to Service-Oriented Computing Using Feature Analysis", (https://scholar.google.com/scholar?a
 Renuka Sindhgatta, Karthikeyan Ponnalagu, "Locating Components Realizing Services in Existing Systems", (https://scholar.google.com/scholar?as_q=Locat
 Saeed Parsa, Leila Ghods, "A new approach to wrap legacy programs into web services", (https://scholar.google.com/scholar?as_q=A+new+approach+to+wrap+
 Haniche Fayçal, Drias Habiba, Mellah Hakima, "Integrating legacy systems in a SOA using an agent based approach for information system agility", (http
 Guo Chenghao, Wang Min, Zhou Xiaoming, "A Wrapping Approach and Tool for Migrating Legacy Components to Web Services", (https://scholar.google.com/sch
 Huaiming Wei, Lijian He, "Constructing the Comprehensive Academic Affairs Management System Based on SOA", (https://scholar.google.com/scholar?as_q=Co
###This article Cites
 A. Bianchi, D. Caivano, V. Marengo and G
 J. Yang, "Web Service Componentization: 
 A. Yeh, D. Harris and H. Reubenstein, "R
 H. M. Sneed, "Wrapping Legacy COBOL Prog
 Z. Zhang and H. Yang, "One-Stone-Two-Bir
 K. Bennett and J. Xu, "Software Services
 T. Costlow, "Web Developers Continue to 
 R. Engelen, G. Gupta and S. Pant, "Devel
 L. Aversano, A. Cimitile, G. Canfora and
 Y. Zou, "Towards a Web-centric Legacy Sy
 J. Chung, K. J. Lin and R. G. Mathieu, "
 D. Linthicum, Next Generation Applicatio
 V. Tzerpos and R. Holt, "MoJo: A Distanc
 A. Deursen and T. Kuipers, "Identifying 
 A. Mehta and G. T. Heineman, "Evolving L
 R. Seacord, D. Plakosh and G. Lewis, Mod
 H. Chang, et al., "QoS-Aware Middleware 
 A. Arsanjani, B. Hailpern, J. Martin and
 S. Tilley, et al., "Adoption Challenges 
 J. Cheesman and J. Daniels, UML Componen
 R. Schwanke, "An Intelligent Tool for Re
 S. Comella-Dorda, K. Wallnau, R. Seacord
 W. Vogels, "Web Services Are not Distrib
 E. Merlo, et al., "Re-engineering User I
 P. Fremantle, S. Weerawarana and R. Khal
 T. A. Wiggerts, "Using Clustering Algori
 C. Szyperski, Component Software beyond 
 R. Koschke and T. Eisenbarth, "A Framewo

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=9444
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'1371920' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 30 Nov.-3 Dec. 2004
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
pdfUrl nil
isNumber 29994
isPromo false
startPage 196
isEphemera false
isACM false
isSAE false
mlTime PT0.052891S
isMorganClaypool false
sourcePdf 01371920.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/9444/29994/01371920.pdf
articleId 1371920
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 30 Nov.-3 Dec. 2004
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1530-1362' ))
mediaPath /mediastore_new/IEEE/content/media/9444/29994/1371920
endPage 203
isStaticHtml true
confLoc Busan, South Korea, South Korea
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/1371920/
accessionNumber 8435463
html_flag true
lastupdate 2020-10-17
pubLink /xpl/conhome/9444/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2004
htmlAbstractLink /document/1371920/
issueLink /xpl/tocresult.jsp?isnumber=29994
displayPublicationTitle 11th Asia-Pacific Software Engineering Conference
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 9444
dbTime 3 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 30 Nov.-3 Dec. 2004
isProduct false
metrics a Dictionary('citationCountPaper'->45 'citationCountPatent'->0 'totalDownloads'->557 )
citationCount 45
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle Incubating services in legacy systems for architectural migration
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Incubating+services+in+legacy+systems+for+architectural+migration&isbn=0-7695-2245-9&publicationDate=2004&author=Z.+Zhang&ContentID=10.1109/APSEC.2004.61&orderBeanReset=true&startPage=196&endPage=203&proceedingName=11th+Asia-Pacific+Software+Engineering+Conference
publicationTitle 11th Asia-Pacific Software Engineering Conference
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-7695-2245-9' ))
xplore-pub-id 9444
xplore-issue 29994
isConference true
displayDocTitle Incubating services in legacy systems for architectural migration
doiLink https://doi.org/10.1109/APSEC.2004.61
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 1371920
