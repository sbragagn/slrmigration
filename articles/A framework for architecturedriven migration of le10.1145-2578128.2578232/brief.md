
#A framework for architecture-driven migration of legacy systems to cloud-enabled software
##Citing
ArticleAuthorRef: (81482654164@ACM;a ZoaArticleReference)14

##Authors
Auhor Name: Ahmad, Aakash
Auhor ID: 81482654164@ACM; 
Auhor Affiliations:  IT University of Copenhagen, Denmark; 
Auhor Name: Babar, Muhammad Ali
Auhor ID: 81100250741@ACM; 
Auhor Affiliations:  University of Adelaide, Australia and IT University of Copenhagen, Denmark; 

##Abstract
With the widespread adoption of cloud computing, an increasing number of organizations view it as an important business strategy to evolve their legacy applications to cloud-enabled infrastructures. We present a framework, named Legacy-to-Cloud Migration Horseshoe, for supporting the migration of legacy systems to cloud computing. The framework leverages the software reengineering concepts that aim to recover the architecture from legacy source code. Then the framework exploits the software evolution concepts to support architecture-driven migration of legacy systems to cloud-based architectures. The Legacy-to-Cloud Migration Horseshoe comprises of four processes: (i) architecture migration planning, (ii) architecture recovery and consistency, (iii) architecture transformation and (iv) architecture-based development of cloud-enabled software. We aim to discover, document and apply the migration process patterns that enhance the reusability of migration processes. We also discuss the required tool support that we intend to provide through our ongoing work in this area.
##Keywords
architecture-centric software migration;cloud computing;reverse engineering and evolution;maintenance and evolution;service-driven architecture;software architecture;reengineering
##Overview
The paper proposes a  process driven architectural oriented migration framework  to cloud computing.###Introduction 	The article gives a quick review on the literature related to cloud migration. 	It details the architectural key features of cloud platforms.	It quickly proposes an overview over the solution's process: a horse shoe that extends the traditional re-engineering horse shoe by augmenting it with details specifics from the use case.### State of research__Other projects__	It compares with on going projects: REMICS, CloudMig.	According to the claims, the proposed framework proposes a planning process for driving the architectural migration (Instead of just caring about cloud providers, cost-risk etc as REMICS).	It also claims that the proposed framework aims to maintain specific architectural properties, instead of basing the approach on adopting cloud architecture as proposed by ARTIST. __Other process models__	Cloud RMM proposes a migration process that is not circular (is not mean to be iterative) 	OSS migration, the authors presented 7 activities to migrate two different open source systems to cloud. __Other Frameworks__	Software reengineering horseshoe	Architecture Driven Modernization	### Framework 	The  proposed horse shoe is basedo n the architecture migration planning as the base of the whole process.	The it counts with other three stages that are mapped from the architecture to the code.	1. Architecture Migration Planning	2. Architecture Recovery and Consistency	3. Architecture transformation	4. Architecture based development	Spin. 	### Evaluation	Proposes an evaluation plan based on cases of study.	For what we can read, to the moment of the paper, there was nothing implemented but only a general process description. 				
##Critics Review
The paper is nice in general. It contains many useful citations. Never the less is based on smoke, there is not code to backup any of their claims. Is as empty as cloudmig/artist and all the other approaches. Never the less, some of the citations are interesting to investigate.Since it does not have much sustain, we should check on the papers citing this paper to see what happened with this approach . 

##References
###This article is cited by
 Cloud Detours: A Non-intrusive Approach for Automatic Software Adaptation to the Cloud (http://link.springer.com/10.1007/978-3-319-24072-5_13)
 The comparison of consumer cloud storage for a storage extension on the e-learning (http://ieeexplore.ieee.org/document/7821930/)
 http://link.springer.com/10.1007/978-3-319-72125-5_9 (http://link.springer.com/10.1007/978-3-319-72125-5_9)
 2016 IEEE Intl Conference on Computational Science and Engineering (CSE) and IEEE Intl Conference on Embedded and Ubiquitous Computing (EUC) and 15th 
###This article Cites
 M. A. Babar Tales of Empirically Underst
 M. A. Chauhan and M. A. Babar. Migrating
 L. Baresi, E. Di Nitto, C. Ghezzi. Towar
 M. A. Babar and M. A. Chauhan. A Tale of
 M. P. Chase, S. M. Christey, D. R. Harri
 A. Winter and J. Ziemann. Model-based Mi
 R. Buyya, C. S. Yeo, S. Venugopal, J. Br
 J. Alonso, L. Orue-Echevarria, M. Escala
 L. Baresi, Luciano, R. Heckel, and S. Th
 B. Wilder. Cloud Architecture Patterns. 
 L. Chen, M. A. Babar, and B. Nuseibeh. C
 A. Stavrou, and G. A. Papadopoulos. Auto
 L. Grunske and E. Lueck. Application of 
 M. Armbrust, A. Fox, R. Griffith, A. D. 
 Capegemining. Business Cloud: The State 
 T. Laszewski, Tom, and P. Nauduri. Migra
 The CloudMIG Approach: Model-Based Migra
 International Journal of Software Engine
 L. Passos, T. R., Valente, M. T. Diniz, 
 N. R. Herbst, S. Kounev and R. Reussner.
 Laws of Software Evolution Revisited. In
 P. Mohagheghi and T. Sæther, Software En
 P. Jamshidi, A. Ahmad, C. Pahl. Cloud Mi
 W. M. Ulrich and P. Newcomb. Information
 . Advanced Information Systems Engineeri
 ISO/IEC 14764:2006 Software Engineering 
 N. Medvidovic and R. N. Taylor. A Classi
 B. J Williams and J. C. Carver. Characte
 R. Kazman, S. G Woods and S. J. Carrière

##Article Metadata
NumberOfPages 8
pages an OrderedCollection(1)
EventType PAPER_CONFERENCE
CollectionTitle WICSA '14 Companion
CollectionNumber 7
PublisherCity New York, NY, USA
Number 7
isbn 9781450325233
EventCity Sydney, Australia
