
#Modernisation or Failure? IT Development Projects in the UK Public Sector
##Citing
ArticleAuthorRef: (Tom Brown@WILEY;a ZoaArticleReference)2

##Authors
Auhor Name: Brown, Tom
Auhor ID: Tom Brown@WILEY; 
Auhor Affiliations:  University of Edinburgh, UK; 

##Abstract
The notable lack of success in UK public sector IT development projects has been recently recognised in policy making circles and considerable efforts are now being made to improve the situation. H...
##Keywords
IT development projects;permanently failing organisations
##Overview
(Brown, 2002) tries to explain the reasons for the failures on the modernisation approaches of the UK public services by using the theory of permanent failure. The interesting relation to learn from this article is the huge financial impact that can have a modernization project that fails.

##References
###This article is cited by
 Afonso Ribeiro, Luísa Domingues, Acceptance of an agile methodology in the public sector, Procedia Computer Science, 10.1016/j.procs.2018.10.083, 138 
 Wendy L Currie, Matthew W Guah, Conflicting Institutional Logics: A National Programme for IT in the Organisational Field of Healthcare, Journal of In
 Carlos F. Gomes, Mahmoud M. Yasin, Michael H. Small, Discerning Interrelationships among the Knowledge, Competencies, and Roles of Project Managers in
 Muhammad Kamal, Vishanth Weerakkody, Zahir Irani, Analyzing the role of stakeholders in the adoption of technology integration solutions in UK local g
 Carlos F. Gomes, Michael H. Small, Mahmoud M. Yasin, Towards excellence in managing the public-sector project cycle: a TQM context, International Jour
 Jill Jameson, Gill Ferrell, Jacquie Kelly, Simon Walker, Malcolm Ryan, Building trust and shared knowledge in communities of e‐learning practice: coll
 Rosemarie Kelly, Gerardine Doyle, Sheila O'Donohoe, Framing Performance Management of Acute‐Care Hospitals by Interlacing NPM and Institutional Perspe
 Amirhossein Eslami Andargoli, e-Health in Australia: A Synthesis of Thirty Years of e-Health Initiatives, Telematics and Informatics, 10.1016/j.tele.2
 Bingqing Yu, Junying Yu, undefined, 2009 International Conference on Management and Service Science, 10.1109/ICMSS.2009.5302107, (1-4), (2009). Crossr
 Wendy L. Currie, David J. Finnegan, The policy‐practice nexus of electronic health records adoption in the UK NHS, Journal of Enterprise Information M
 Sandeep Kumar Gupta, Angappa Gunasekaran, Shivam Gupta, Surajit Bag, David Roubaud, Systematic Literature Review of Project Failures: Current Trends a
 Stephen M. Davies, The Experimental Computer Programme: The First Computing Initiative for the National Health Service in England, IEEE Annals of the 
 Daniel Peckham, Electronic patient records, past, present and future, Paediatric Respiratory Reviews, 10.1016/j.prrv.2016.06.005, 20 , (8-11), (2016).
 Muhammad Mustafa Kamal, Vishanth Weerakkody, Examining the Role of Stakeholder’s in Adopting Enterprise Application Integration Technologies in Local 
 Wendy L. Currie, From Professional Dominance to Market Mechanisms: Deinstitutionalization in the Organizational Field of Health Care, Information Syst
 Ron Hodges, Suzana Grubnic, LOCAL AUTHORITY E‐GOVERNMENT PARTNERSHIPS IN ENGLAND: A CASE STUDY, Financial Accountability & Management, 10.1111/j.1468-
 Nikhil Shah, Welfare and technology in the network society—Concerns for the future of welfare, Futures, 10.1016/j.futures.2012.04.004, 44 , 7, (659-66
 Muhammad Mustafa Kamal, Vishanth Weerakkody, Examining the Role of Stakeholder’s in Adopting Enterprise Application Integration Technologies in Local 
 Sandra Cohen, Efrosini Kaimenaki, Yannis Zorgios, ASSESSING IT AS A KEY SUCCESS FACTOR FOR ACCRUAL ACCOUNTING IMPLEMENTATION IN GREEK MUNICIPALITIES, 
 Angus G. Yu, undefined, 2009 International Conference on Management and Service Science, 10.1109/ICMSS.2009.5302061, (1-4), (2009). Crossref (10.1109/
 S. K. Mahama Ewurah, The Concept of eGovernment: ICT Policy Guidelines for the Policy Makers of Ghana, Journal of Information Security, 10.4236/jis.20
 IRVINE LAPSLEY, New Public Management: The Cruellest Invention of the Human Spirit?, Abacus, 10.1111/j.1467-6281.2009.00275.x, 45 , 1, (1-21), (2009).
 Irvine Lapsley, Peter Miller, Neil Pollock, Foreword Management Consultants – Demons or Benign Change Agents?, Financial Accountability & Management, 
 Carlos F. Gomes, Mahmoud M. Yasin, João V. Lisboa, Project management in the context of organizational change, International Journal of Public Sector 
 Henning Rode, To Share or not to Share: The Effects of Extrinsic and Intrinsic Motivations on Knowledge-sharing in Enterprise Social Media Platforms, 
 Michela Arnaboldi, Irvine Lapsley, Martina Dal Molin, Modernizing public services: subtle interplays of politics and management, Journal of Accounting
###This article Cites

##Article Metadata
pages an OrderedCollection(363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 379 380 381)
pdfUrl https://onlinelibrary.wiley.com/doi/pdf/10.1111/1468-0408.00139
issue 4
volume 17
journal Financial Accountability & Management
isnn 1468-0408
AbstractUrl https://onlinelibrary.wiley.com/doi/abs/10.1111/1468-0408.00139
Language en
