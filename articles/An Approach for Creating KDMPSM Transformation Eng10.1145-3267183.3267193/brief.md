
#An Approach for Creating KDM2PSM Transformation Engines in ADM Context: The RUTE-K2J Case
##Citing
ArticleAuthorRef: (99659305428@ACM;a ZoaArticleReference)18

##Authors
Auhor Name: Angulo, Guisella
Auhor ID: 99659305428@ACM; 
Auhor Affiliations:  UFSCar, São Carlos, Brazil; 
Auhor Name: Martín, Daniel San
Auhor ID: 99659305579@ACM; 
Auhor Affiliations:  UFSCar, São Carlos, Brazil; 
Auhor Name: Santos, Bruno
Auhor ID: 99658665264@ACM; 
Auhor Affiliations:  UFSCar, São Carlos, Brazil; 
Auhor Name: Ferrari, Fabiano Cutigi
Auhor ID: 81502650234@ACM; 
Auhor Affiliations:  UFSCar, São Carlos, Brazil; 
Auhor Name: Camargo, Valter Vieira de
Auhor ID: 81350603254@ACM; 
Auhor Affiliations:  UFSCar, São Carlos, Brazil; 

##Abstract
Architecture-Driven Modernization (ADM) is a type of software reenginering that employs standard metamodels along the process and deals with the whole system architecture. The main metamodel is the Knowledge-Discovery Metamodel (KDM), which is language, platform independent and it is able to represent several aspects of a software system. Although there is much research effort in the reverse engineering phase of ADM, little have been published around the forward engineering one; mainly on the generation of Platform-Specific Models (PSM) from KDM. This phase is essential as it belongs to the final part of the horseshoe cycle, completing the reengineering process. However, the lack of research and the absence of tooling support hinders the industrial adoption of ADM. Therefore, in this paper we propose an approach to support engineers in creating Transformation Engines (TE) from KDM to any other PSM. This approach was emerged from the experience in creating a TE called RUTE-K2J, which aims at generating Java Model from KDM. The transformation rules of RUTE-K2J were tested considering sets of common code structures that normally appears when modernizing systems. The test cases have shown the transformation rules were able to generate correctly 92% of the source code that was submitted to the transformation.
##Keywords
Model transformation;PSM;Java Model;KDM

##References
###This article is cited by
 Formalizing Natural Languages with NooJ 2018 and Its Natural Language Processing Applications. (http://link.springer.com/10.1007/978-3-030-10868-7_19)
###This article Cites
 Robert France and Bernhard Rumpe. 2007. 
 Feliu Trias, Valeria de Castro, Marcos L
 Roberto Rodríguez-Echeverría, José María
 2018. ADM Vendor Directory Listing. http
 Christian Wagner. 2014. Model-Driven Sof
 Keith Bennett. 1995. Legacy Systems: Cop
 Javier Canovas and Jesus Molina. 2010. A
 Daniel San Martín Santibáñez, Rafael Ser
 Claes Wohlin, Per Runeson, Martin Höst, 
 Javier Luis Cánovas Izquierdo and Jesús 
 Ricardo Perez-Castillo, Ignacio Garcia-R
 Ricardo Pérez-Castillo, Ignacio García R
 Feliu Trias, Valeria de Castro, Marcos L
 2006. Eclipse ATL Project. https://proje
 Christian Wulf, Sören Frey, and Wilhelm 
 Ricardo Pérez-Castillo, Ignacio García-R
 2013. Eclipse Epsilon Project. http://ww
 Object Management Group. 2009. Architect
 Rafael S Durelli, Daniel SM Santibáñez, 
 Harry M Sneed. 2005. Estimating the cost
 Franck Barbier, Gaëtan Deltombe, Olivier
 Eirini Kalliamvakou, Georgios Gousios, K
 André de Souza Landi, Fernando Chagas, B
 Tom Mens and Pieter Van Gorp. 2006. A Ta
 Jorge Moratalla, Valeria de Castro, Marc
 Hugo Bruneliere, Jordi Cabot, Frédéric J
 2006. Eclipse Acceleo Project. http://ww
 Olegas Vasilecas and Kestutis Normantas.

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(92)
EventCity Sao Carlos, Brazil
CollectionTitle SBCARS '18
NumberOfPages 10
isbn 9781450365543
pdfUrl https://dl.acm.org/doi/pdf/10.1145/3267183.3267193
