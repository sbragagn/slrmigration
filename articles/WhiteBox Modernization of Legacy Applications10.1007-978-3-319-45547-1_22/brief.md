
#White-Box Modernization of Legacy Applications
##Citing
ArticleAuthorRef: (180995498@SPRINGER;a ZoaArticleReference)16

##Authors
Auhor Name: Garcés, Kelly
Auhor ID: 180995498@SPRINGER; 
Auhor Affiliations:  School of Engineering, Department of Systems and Computing Engineering; 
Auhor Name: Casallas, Rubby
Auhor ID: 166686328@SPRINGER; 
Auhor Affiliations:  School of Engineering, Department of Systems and Computing Engineering; 
Auhor Name: Álvarez, Camilo
Auhor ID: 191793770@SPRINGER; 
Auhor Affiliations:  School of Engineering, Department of Systems and Computing Engineering; 
Auhor Name: Sandoval, Edgar
Auhor ID: 6210843@SPRINGER; 
Auhor Affiliations:  School of Engineering, Department of Systems and Computing Engineering; 
Auhor Name: Salamanca, Alejandro
Auhor ID: 56661557@SPRINGER; 
Auhor Affiliations:  Asesoftware Ltda.; 
Auhor Name: Melo, Fabián
Auhor ID: 119072034@SPRINGER; 
Auhor Affiliations:  Asesoftware Ltda.; 
Auhor Name: Manuel, Juan
Auhor ID: 260158721@SPRINGER; 
Auhor Affiliations:  Asesoftware Ltda.; 

##Abstract
Software modernization consists of transforming legacy applications into modern technologies, mainly to minimize maintenance costs. This transformation often produces a new application that is a poor copy of the legacy due to the degradation of quality attributes, for example. This paper presents a white-box transformation approach that changes the application architecture and the technological stack without losing business value and quality attributes. This approach obtains a technology agnostic model from the original sources, such a model facilitates the architecture configuration before performing the actual transformation of the application into the new technology. The architecture for the new application can be configured considering aspects such as data access, quality attributes, and process. We evaluate our approach through an industrial case study, the gist of which is the transformation of Oracle Forms applications—where the presentation layer is highly coupled to the data access layer—to Java technologies.
##Keywords
Industrial case study ;Quality attributes ;Oracle forms ;Multi-tier architecture ;Configuration ;Model-driven techniques 
##Overview
The author does as Model driven development and architecture from OMG. It uses the specified model for UI's.
Proposes a transformation form a language model to a meta model, then from the meta model to a "modern code" output.
It acknowledge details of implementation. It divides the problem layers: UI business and persistence. 
As evaluation measures the productivity of four developers with experience in java (the destination technology of the migration).  They split them in two teams of two. Each team has a senior and a junior.
	


##References
###This article is cited by
###This article Cites
 unknown title (http://www.vgosoftware.co
 O (10.1007/s10515-013-0130-2)
 N (https://scholar.google.com/scholar?q=
 B (10.1016/S0164-1212(96)00171-9)
 unknown title (http://www.composertechno
 unknown title (http://www.turbo-enterpri
 unknown title (https://www.pitss.com/)
 T (10.1023/A:1022697422240)
 K (https://scholar.google.com/scholar?q=
 J (10.1109/MS.2010.61)
 unknown title (http://www.oracle.com/tec
 L (10.1007/11877028_8)
 unknown title (http://qafe.com/)
 W (http://scholar.google.com/scholar_loo
 unknown title (https://www.renaps.com/)
 unknown title (http://www.kumaran.com/)

##Article Metadata
