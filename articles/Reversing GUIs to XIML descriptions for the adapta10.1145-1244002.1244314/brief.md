
#Reversing GUIs to XIML descriptions for the adaptation to heterogeneous devices
##Citing
ArticleAuthorRef: (81100137232@ACM;a ZoaArticleReference)7

##Authors
Auhor Name: Santo, Giuseppe Di
Auhor ID: 81100137232@ACM; 
Auhor Affiliations:  University of Sannio, Via Traiano, Benevento, Italy; 
Auhor Name: Zimeo, Eugenio
Auhor ID: 81100512682@ACM; 
Auhor Affiliations:  RCOST University of Sannio, Via Traiano, Benevento, Italy; 

##Abstract
The spread of Personal Wireless Devices (PWDs) has raised the need to migrate existing applications to these new environments. Desktop applications often exhibit complex user interfaces and are too large and resource demanding to be executed on devices with limited resources without changing the application code. Current research efforts are mainly focused on Web applications whose user interfaces are specifically designed for multi-platform environments through platform-independent models. On the contrary, little effort has been made to support the migration of applications with component-based GUIs towards PWD environments. This paper presents a tool for reverse engineering Java GUIs through their transformations to XIML-based abstract descriptions. The resulting descriptions are used by the TCPTE framework to be rendered into different GUIs, which are dynamically adapted to heterogeneous devices on the basis of their profile communicated at request time.
##Keywords
graphical user interfaces;reverse engineering;software architectures;middleware

##References
###This article is cited by
 Proceedings of the 2008 ACM symposium on Applied computing (10.1145/1363686.1364150)
 Journal of Software Maintenance and Evolution: Research and Practice (http://doi.wiley.com/10.1002/smr.400)
 2013 IEEE Seventh International Conference on Research Challenges in Information Science (RCIS). (http://ieeexplore.ieee.org/document/6577696/)
 Portability Approaches for Business Web Applications to Mobile Devices: A Systematic Mapping (http://link.springer.com/10.1007/978-3-030-05532-5_11)
###This article Cites
 Giulio Mori, Fabio Paternò, Carmen Santo
 Gerardo Canfora, Giuseppe Di Santo, Euge
 Gottfried Zimmermann, Gregg Vanderheiden
 Laurent Bouillon, Jean Vanderdonckt, Kwo
 Giulio Mori, Fabio Paternò, Carmen Santo
 Rajesh Krishna Balan, Mahadev Satyanaray
 Marc Abrams, Constantinos Phanouriou, Al
 Gerardo Canfora, Giuseppe Di Santo, Euge
 Angel Puerta, Jacob Eisenstein, "XIML: A
 G. Meszaros, "Pattern: Half-Object + Pro

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(1456)
EventCity Seoul, Korea
CollectionTitle SAC '07
NumberOfPages 5
isbn 1595934804
