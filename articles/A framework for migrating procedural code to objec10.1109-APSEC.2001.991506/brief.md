
#A framework for migrating procedural code to object-oriented platforms
##Citing
ArticleAuthorRef: (37275730500@IEEE;a ZoaArticleReference)2

##Authors
Auhor Name: Ying Zou, Ying Zou
Auhor ID: 37275730500@IEEE; 
Auhor Affiliations:  
Auhor Name: Kontogiannis, K.
Auhor ID: 37277151500@IEEE; 
Auhor Affiliations:  

##Abstract
With the rapid growth of the Internet and pervasive computing activities, the migration of back-end legacy systems to network centric environments has become a focal point for researchers and practitioners alike. To leverage back-end legacy services into Web-enabled environments, this paper proposes an incremental and iterative migration framework where legacy procedural source code is reengineered into an object-oriented platform. The reengineering framework allows for the representation of the legacy source in the form of XML based annotated abstract syntax trees. Consequently, the extraction of an object-oriented model from the original source code is based on the analysis of source code features in the original system that can be used to identify classes, associations, aggregations, and polymorphic patterns in the new target system.
##Keywords
procedural code migration;XML;Computer networks;polymorphic patterns;classes;Application software;Software architecture;incremental migration framework;hypermedia markup languages;aggregations;network centric environments;associations;systems re-engineering;object-oriented programming;Internet;Pervasive computing;pervasive computing activities;legacy procedural source code reengineering;iterative migration framework;back-end legacy system migration;Object oriented modeling;Web-enabled environments;object-oriented platforms;Pattern analysis;Web and internet services;Java;XML based annotated abstract syntax trees;IP networks;software portability
##Overview
The article proposes an approach for migrating procedural to OO code The article starts explaining the usage of XML for storing a model of the source program. It remarks the utility of such a technologically agnostich standard. And it explains how does it map the syntactical elements of the origin languages and merge different languages in a single representation.Nevertheless the paper has some real contribution, the heuristics to map Classes, associations and overload. The paper ends with experiments over the WELTAB system created in 1970 to support collection reporting and certification for US Federal elections.
##Class Creation
In this section it recognices different kind of heuristics for class creation__Private member identification__-Data Type analysis	A structure such as struct or union represents a cohesive state. It could easily be used as the class state.-Variable analysis	There are three scopes to access variables in a program 	According to their accessing scope they can be defined to a different visibility	__Method identification__- Parameter type analysis. 	A received parameter if only one it could be inferred as "this" 	If more than one we cannot say much- Return type analysis	A return type, as the parameter type could be used to infer the class where this function belongs as a method. 	If a parameter type matches with the return type should have more importance to decide. - Variable usage analysis	Analyse if the function defines variables of some specific type. - Metrics 	The metrics used principally are Flow and function Point.	Reducinf the Flow and Function point metrics is a way to reduce the amount of information going around. 
##Class association discovery
The main features in OO are inhertiance polymorphism and overloading. ###Inheritance__Data field cloning__	If two structures differ by one attribute, they may be redefined as superclass / subclass__Data field mapping__	Certain functions copy values from one data structure to an otherone .That may mean that these two structures share behaviour. __Function code cloning__	Certain functions have exactly the same code with different types.  These is typical in languages such as C. 	These of course means that we have a really good candidate for inheritance __Data type casting__	When casting a type to an other, it means that these two types are somehow polimorphic in some functional context.__Anonymous Union type__	When using union, we cannot use all the fields at the same time, but we know that the code related with each field is highly related.		###Polymorphism __Switch statement replacement__	Is well known that in OO the usage of switch is a code smell related with the lack of polymorphism. When finding usages of switch if all the cases are mutually exclusive and possible to extract to a function, therefore we can refactorize it to be dispatched to different objects.__Conditional statement replacement__	Similar to the switch. __Function Pointer__	The usage of function pointer surely means different strategies or criterias. This can be used to infer polymorphism .__Generic pointer parameters replacement__	Void * is used to accept a wide kind of types. The tracking of usage of functions that receive void* may allow to bind different types to an specific functionality. 	

##References
###This article is cited by
 R. Al-Ekram, K. Kontogiannis, "An XML-based framework for language neutral program representation and generic analysis", (https://scholar.google.com/s
 Marco Trudel, Carlo A. Furia, Martin Nordio, Bertrand Meyer, Manuel Oriol, "C to O-O Translation: Beyond the Easy Stuff", (https://scholar.google.com/
 Ying Zou, K. Kontagiannis, "Incremental transformation of procedural systems to object oriented platforms", (https://scholar.google.com/scholar?as_q=I
 Y. Ping, K. Kontogiannis, "Refactoring Web sites to the controller-centric architecture", (https://scholar.google.com/scholar?as_q=Refactoring+Web+sit
###This article Cites
 "Refine/C Programming's Guide" in , Reas
 Michael W. Godfrey, "Defining Transformi
 G.A. Di De Lucia, A.R. Fasolino Lucca, P
 K. Kontogiannis and P. Patil, "Evidence 
 Fowler Martin, "Refactoring: Improving t
 R. Koschke, J.-F. Girard and M. Würthner
 Letha H. Etzkorn and Carl G. Davis, "Aut
 R. C. Holt et al., "GXL: Toward a Standa
 P. Patil, Y. Zou, K. Kontogiannis and J.
 E. Mamas and K. Kontogiannis, "Towards P
 Jörg Czeramski et al., "Data Exchange in

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=7784
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('displayTextWhenOtherFormatPricingNotAvailable'->'The purchase and pricing options for this item are unavailable. Select items are only available as part of a subscription package. You may try again later or <a href=''https://ieeexplore.ieee.org/xpl/contact'' target=''_blank''>contact us</a> for more information.' 'displayTextWhenPdfPricingNotAvailable'->'The purchase and pricing options for this item are unavailable. Select items are only available as part of a subscription package. You may try again later or <a href=''https://ieeexplore.ieee.org/xpl/contact'' target=''_blank''>contact us</a> for more information.' 'mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfoAvailable'->false 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 4-7 Dec. 2001
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
pdfUrl nil
isNumber 21387
isPromo false
startPage 390
isEphemera false
isACM false
isSAE false
mlTime PT0.042168S
isMorganClaypool false
sourcePdf 991506.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/7784/21387/00991506.pdf
articleId 991506
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 4-7 Dec. 2001
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1530-1362' ))
mediaPath /mediastore_new/IEEE/content/media/7784/21387/991506
endPage 399
isStaticHtml true
confLoc Macao, China, China
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/991506/
accessionNumber 7176519
html_flag true
lastupdate 2020-08-24
pubLink /xpl/conhome/7784/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2001
htmlAbstractLink /document/991506/
issueLink /xpl/tocresult.jsp?isnumber=21387
displayPublicationTitle Proceedings Eighth Asia-Pacific Software Engineering Conference
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 7784
dbTime 3 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 4-7 Dec. 2001
isProduct false
metrics a Dictionary('citationCountPaper'->4 'citationCountPatent'->2 'totalDownloads'->140 )
citationCount 4
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle A framework for migrating procedural code to object-oriented platforms
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=A+framework+for+migrating+procedural+code+to+object-oriented+platforms&isbn=0-7695-1408-1&publicationDate=2001&author=Ying+Zou&ContentID=10.1109/APSEC.2001.991506&orderBeanReset=true&startPage=390&endPage=399&proceedingName=Proceedings+Eighth+Asia-Pacific+Software+Engineering+Conference
publicationTitle Proceedings Eighth Asia-Pacific Software Engineering Conference
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-7695-1408-1' ))
xplore-pub-id 7784
xplore-issue 21387
isConference true
displayDocTitle A framework for migrating procedural code to object-oriented platforms
doiLink https://doi.org/10.1109/APSEC.2001.991506
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 991506
