
#Modernization of Legacy Systems: A Generalised Roadmap
##Citing
ArticleAuthorRef: (99659229118@ACM;a ZoaArticleReference)15

##Authors
Auhor Name: Jain, Suman
Auhor ID: 99659229118@ACM; 
Auhor Affiliations:  Computer Science Department, Thapar University, Patiala, India; 
Auhor Name: Chana, Inderveer
Auhor ID: 81467670584@ACM; 
Auhor Affiliations:  Computer Science Department, Thapar University, Patiala, India; 

##Abstract
A number of key organizations are sustaining the decades old complex legacy systems despite their types of services and operating environment. Cloud computing provides numerous processes, tools and methods to emphasize upon the pay-as-per-use utility models. The utter need to modernize these older systems leads to modernization processes involving the vision to leverage the cloud computing benefits. The proposed modernization framework includes decision support module followed by the elaboration of available modernization approaches. This legacy problem is one of the main challenges for cloud computing to deal with. When re-architecting and reengineering these legacy systems, parallel computing approaches also need to be assessed for resource optimization in cloud environments.
##Keywords
Cloud computing;Modernization;MoDisco;Reengineering;Migration;Decision making module;Service oriented architecture
##Overview
This is a survey paper. It looks to understand what are the available approaches for modernization with cloud perspective? How to propose a generalised framework for assessment of legacy systems to adapt cloud? The survey proposes a roadmap based on the literature .- Decision making module	1.  Source application assesment	  *  Application Components	  * Complexity	  * Application Layer 	2. Quality of Services 	  * Performance	  * Multi-Tenancy 	  * Scalability   	3.  Target cloud architecture assesment 	  * Deployment model	  * Pricing policy	   * Cloud Vendor- Modernization approaches	1. Replacement of legacy system	2. Black box wrapping	3. Reengineering / Redevelopment	  * System Re-Engineering	  * Incremental Re-engineering	4. Migration to new cloud environment- Implementation process	The process proposed is iterative as spiral IBM.		

##References
###This article is cited by
 Silva, E. A., & Lucredio, D. (2012). Software Engineering for the Cloud: a Research Roadmap. Brazilian Symposium on Software Engineering. (10.1109/SBE
 Olsem, M. (1998). An Incremental Approach to Software Systems Re-engineering. Software Maintenance: Research and Practice. (10.5555/294964.294966)
 D. Jeffrey and G. Sanjay, "Mapreduce: simplified data processing on large clusters," vol. 51, Communications of the ACM, 2008, pp. 107--113. (10.1145/
 Antoun, M., & Coelho, W. (n.d.). A Case Study in Incremental Architecture based Re-engineering of a Legacy Application. IEEE. (http://scholar.google.c
 Zhao, J.-F., & Zhou, J.-T. (2014). Strategies and Methods for Cloud Migration. International Journal of Automation and Computing, 143--152. (10.1007/s
 Armbrust, M., Fox, A., Griffith, R., Joseph, A. D., Katz, R., Konwinski, A., Zaharia, M. (2009). Above the Clouds: A Berkeley view of cloud computing.
 Buyya, R., Yeo, C. S., Venugopal, S., Broberg, J., & Brandic, I. (2009). Cloud Computing and emerging IT platforms: Vision, hype, and reality for deli
 Pahl, C., & Xiong, H. (2013). Migration to PaaS Clouds-Migration Process and Architectural concerns. 7th IEEE International Symposioum on the Maintena
 J. Varia, "Migrating your Existing Applications to the AWS Cloud," October 2010. {Online}. Available: htttp://www.amazon.com. (http://scholar.google.c
 Frey, S., & Hasselbring, W. (2011), An Extensible Architecture for Detecting violations of a Cloud Environment Constraints during Legacy Software Syst
 M. L. Brodie and M. Stonebraker, "Migrating Legacy Systems: Gateways, Interfaces & the Incremental Approach," Morgan Kaufmann Publishers Inc., San Fra
 Dillon, T., Wu, C., & Chang, E. (2010). Cloud Computing: Issues and Challanges. 24th IEEE International Conference on Advanced Information Networking 
 R. J. Anderson, E. W. Mayr and M. K. Warmuth, "Parallel Approximation Algorithms for Bin Packing," Information and Computation, vol. 82, no. 3, pp. 26
 Muralidaran, N., Sarda, N. L., & Srivastava, S. C. (2014). Ever Changing Business and Technology Landscape: Transformation Alternatives. Institute of 
###This article Cites
 Silva, E. A., & Lucredio, D. (2012). Sof
 Olsem, M. (1998). An Incremental Approac
 D. Jeffrey and G. Sanjay, "Mapreduce: si
 Antoun, M., & Coelho, W. (n.d.). A Case 
 Zhao, J.-F., & Zhou, J.-T. (2014). Strat
 Armbrust, M., Fox, A., Griffith, R., Jos
 Buyya, R., Yeo, C. S., Venugopal, S., Br
 Pahl, C., & Xiong, H. (2013). Migration 
 J. Varia, "Migrating your Existing Appli
 Frey, S., & Hasselbring, W. (2011), An E
 M. L. Brodie and M. Stonebraker, "Migrat
 Dillon, T., Wu, C., & Chang, E. (2010). 
 R. J. Anderson, E. W. Mayr and M. K. War
 Muralidaran, N., Sarda, N. L., & Srivast

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(62)
EventCity Allahabad, India
CollectionTitle ICCCT '15
NumberOfPages 6
isbn 9781450335522
