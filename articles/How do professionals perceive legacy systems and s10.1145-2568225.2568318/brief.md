
#How do professionals perceive legacy systems and software modernization?
##Citing
ArticleAuthorRef: (81496686894@ACM;a ZoaArticleReference)14

##Authors
Auhor Name: Khadka, Ravi
Auhor ID: 81496686894@ACM; 
Auhor Affiliations:  Utrecht University, Netherlands; 
Auhor Name: Batlajery, Belfrit V.
Auhor ID: 88158859457@ACM; 
Auhor Affiliations:  Utrecht University, Netherlands; 
Auhor Name: Saeidi, Amir M.
Auhor ID: 81496686961@ACM; 
Auhor Affiliations:  Utrecht University, Netherlands; 
Auhor Name: Jansen, Slinger
Auhor ID: 81100103507@ACM; 
Auhor Affiliations:  Utrecht University, Netherlands; 
Auhor Name: Hage, Jurriaan
Auhor ID: 81100273210@ACM; 
Auhor Affiliations:  Utrecht University, Netherlands; 

##Abstract
 Existing research in legacy system modernization has traditionally focused on technical challenges, and takes the standpoint that legacy systems are obsolete, yet crucial for an organization's operation. Nonetheless, it remains unclear whether practitioners in the industry also share this perception. This paper describes the outcome of an exploratory study in which 26 industrial practitioners were interviewed on what makes a software system a legacy system, what the main drivers are that lead to the modernization of such systems, and what challenges are faced during the modernization process. The findings of the interviews have been validated by means of a survey with 198 respondents. The results show that practitioners value their legacy systems highly, the challenges they face are not just technical, but also include business and organizational aspects. 
##Keywords
Legacy Modernization;Grounded Theory;Legacy Systems;Empirical Studies
##Overview
The paper runs a ground truth study on the professionals  perception of software modernization. The study is runned with 26 different participants. The participants come from different domains with an average experience of 19 years (from 5 to 43). The study was conducted as a semistructured interview, and after corroborated by a poll on 176 different professionals. The article exposes the discussions and opinions. And for the numbers it is based on the polls.  
##Legacy Systems
__What are them?__ * Old systems (20 - 30 years)* Core systems * System that do not belong to the strategic technological goals.__There is a relation between language and legacy?__About half of the people thinks that there is not relation between language and the fact that a system is legacy. On the other half, we find different ideas of which languages mean legacy:* Visual Basic 12%* RPG 10%* COBOL 47%* PL/I 14%* Assembler 17%###Benefits of Legacy systems__Business Critical__	76.7%  agree on the fact that legacy system is maintained, specially because is important to the business. 	"By definition a legacy system is business critical. A system that is old and obsolote and is not busines critical would never reach the status of legacy" 	__Proven Technology__ 	52.8% agree on the fact that the legacy system reaches stability. After many years of usage it is safe to call it proven technology. And at some point proven availability. 	"Most of the time its a provent technology. AS400 is stable. It works 24/7 and it is quite good. it is stable"	"They are available. And they run 24/7" 	__Reliable Systems__	52.3% agree on the fact that legacy systems are oftenly reliable. 	"It is reliable. People know how to use it. All the problems have been fixed over the years from it. So technical problems usually are not there"	__Performance__	24.4% agree on the good performance of legacy systems.	In the interviews there were also different positions:	"I think ten thousand people are doing airlines booking with this little processing power. So performance is never an issue in a legacy system" 	"The performance is poor" or "the performance is just enough" 
##Drivers For Modernization
What are the perceived benefits of a modernization. __To Remain Agile to Change__	(85% agree to do it to gain flexibility) Despite the fact that Legacy are business critical, there are inflexible to change.  	(43% agree to do it to gain possiblity of opportunity) It makes harder to adapt to create new business oportunities. 	(71% agree to do it to get a faster time to market) It slow down the time to market. __High Maintenance Cost__	80.4% agree that reduce the maintainance cost is a driver: "I am quite sure that Ican run a similar environment agains lower cost if i could use a state of the art standard ERP. I would not need anybody to maintain it"__Lack of Knowledge__	60% agree that the lack of knowledge is only to be reduced by modernization "The issue is that there is less knowleadgeable pepple in the organization. Most of them left. The other characteristic is the lack of documentation" 		__Prone to Failure__	25.7% Agree that legacy systems are prone to failure. Even knowing that they are reliable: "When you environment runs out of support then it is really dying and if that is true, then you are already too late" 	
##Challenges of Modernization
__Time Constraints to Finish Modernization__	63.3% agree that the first challenge is time. 	"Your biggest problem is an availability of resources: documentation , experts. Availability of money an to some extent, availability of time."__Data Migration__	56% of the survey agreest that data migration is one of the challenges.	"The main risk in modernization is that the data migration cannot be done perfectly. Errors are made and you have some risk that your new system is disturbed after modernization" __Complex System Architecture__	56.4% agree on the challenge that represent the difficult to test.	46.3% on the challenge of a non evolvable architecture. 	"Not only the development part, we have to think about the architecture of the system also,	because if people are bumping against the architecture changing the earlier architecture" 		__Lack of Knowledge__	60.7% agree that  the lack of knowledge is a challenge.	"You also need people that understand database environment and operative systes, middleware, enterpriser service bus of architectur, and business  functionality"	__Difficult to Extract and Prioritize Business Logic__	50.3% agree that this is a challenge	"The project team has to extract the exact functionality. It could be difficult to extract, to document and to implement it properly" 	__Resistance from Organization__	49% agree	"Sometimes people do not like changes. Not only in the business organizations, but also in IT organizations" __Addressing Soft Factors of Modernization__	45.3% agree 	"You have to come up with the business case that says what is my current cost, what is the cost of migration and what the new cost migration will be. Finally you have to predict the ROI. A business case can have components as enhance performance, improve maintenance." 	

##References
###This article is cited by
 http://services.igi-global.com/resolvedoi/resolve.aspx?doi=10.4018/978-1-4666-8510-9.ch012 (10.4018/978-1-4666-8510-9.ch012)
 Experiences from reengineering and modularizing a legacy software generator with a projectional language workbench (10.1145/2934466.2962733)
 Mining Association Rules from Code (MARC) to support legacy software management (http://link.springer.com/10.1007/s11219-019-09480-3)
 2017 11th International Conference on Software, Knowledge, Information Management and Applications (SKIMA). (http://ieeexplore.ieee.org/document/82941
 Proceedings of the 18th ACM Conference on Computer Supported Cooperative Work & Social Computing (10.1145/2675133.2675232)
 2020 IEEE 27th International Conference on Software Analysis, Evolution and Reengineering (SANER). (https://ieeexplore.ieee.org/document/9054813/)
 ACM Transactions on Management Information Systems (TMIS) (10.1145/3230713)
 An Open Source Approach for Modernizing Message-Processing and Transactional COBOL Applications by Integration in Java EE Application Servers (http://
 Proceedings of the 41st International Conference on Software Engineering: Software Engineering in Practice (10.1109/ICSE-SEIP.2019.00015)
 2019 6th International Conference on Research and Innovation in Information Systems (ICRIIS). (https://ieeexplore.ieee.org/document/9073628/)
 Managing legacy system costs: A case study of a meta-assessment model to identify solutions in a large financial services company (https://linkinghub.
 Does software modernization deliver what it aimed for? A post modernization analysis of five software modernization case studies (10.1109/ICSM.2015.73
 Extracting Candidates of Microservices from Monolithic Application Code (https://ieeexplore.ieee.org/document/8719439/)
 2020 IEEE 27th International Conference on Software Analysis, Evolution and Reengineering (SANER). (https://ieeexplore.ieee.org/document/9054823/)
 2017 IEEE 24th International Conference on Software Analysis, Evolution and Reengineering (SANER). (http://ieeexplore.ieee.org/document/7884669/)
 An approach to error correction in program code using dynamic optimization in a virtual execution environment (10.1007/s11227-015-1616-4)
 Development of dynamic protection against timing channels (10.1007/s10207-016-0356-7)
 http://journals.sagepub.com/doi/10.1177/0162243919900965 (10.1177/0162243919900965)
 The Strategic Technical Debt Management Model: An Empirical Proposal (http://link.springer.com/10.1007/978-3-030-47240-5_13)
 Proceedings of the 2nd international Conference on Big Data, Cloud and Applications (10.1145/3090354.3090385)
 Proceedings of the 2nd ACM SIGPLAN International Workshop on Comprehension of Complex Systems (10.1145/3141842.3141843)
 Architecture-Driven Development of an Electronic Health Record Considering the SOAQM Quality Model (http://link.springer.com/10.1007/s42979-020-00150-
 Proceedings of the 2015 IEEE International Conference on Software Maintenance and Evolution (ICSME) (10.1109/ICSM.2015.7332497)
 2016 IEEE 23rd International Conference on Software Analysis, Evolution and Reengineering (SANER). (http://ieeexplore.ieee.org/document/7476697/)
###This article Cites
 W. S. Adolph. Cash cow in the tar pit: R
 E. Stroulia, M. El-Ramly, and P. Sorenso
 R. Khadka, A. Saeidi, S. Jansen, J. Hage
 I. Warren and D. Avallone. The renaissan
 K. H. Bennett and V. T. Rajlich. Softwar
 J. Bisbal, D. Lawless, B. Wu, J. Grimson
 J. Bisbal, D. Lawless, B. Wu, and J. Gri
 H. M. Sneed. Software renewal: A case st
 B. G. Glaser and A. L. Strauss. The disc
 W. Hasselbring, R. Reussner, H. Jaekel, 
 M. L. Brodie and M. Stonebraker. DARWIN:
 T. Sucharov and P. Rice. The burden of l
 M. L. Brodie and M. Stonebraker. Migrati
 M. Goedicke and U. Zdun. Piecemeal legac
 E. C. Arranga and F. P. Coyle. Cobol: Pe
 A. A. Almonaies, J. R. Cordy, and T. R. 
 P. Thiran, J.-L. Hainaut, G.-J. Houben, 
 N. Golafshani. Understanding reliability
 R. C. Seacord, D. Plakosh, and L. A. Gra
 G. Canfora, A. Cimitile, A. De Lucia, an
 SQuaRE. Systems and software quality req
 R. Khadka, A. Saeidi, A. Idu, J. Hage, a
 M. Razavian and P. Lago. A frame of refe
 A. Quilici. Reverse engineering of legac
 N. Veerman. Revitalizing modifiability o
 N. H. Weiderman, J. K. Bergey, D. B. Smi
 M. Torchiano, M. Di Penta, F. Ricca, A. 
 A. Bianchi, D. Caivano, V. Marengo, and 
 S. Easterbrook, J. Singer, M.-A. Storey,
 L. A. Belady and M. M. Lehman. A model o
 M. Colosimo, A. D. Lucia, G. Scanniello,
 S. Murer, B. Bonati, and F. J. Furrer. M
 B. Cornelissen, A. Zaidman, A. van Deurs
 M. Mortensen, S. Ghosh, and J. M. Bieman
 A. De Lucia, R. Francese, G. Scanniello,
 J. Lavery, C. Boldyreff, B. Ling, and C.
 H. M. Sneed. Risks involved in reenginee
 B. W. Weide, W. D. Heym, and J. E. Holli
 M. Razavian and P. Lago. A survey of SOA
 M. L. Brodie. The promise of distributed
 B. V. Batlajery, R. Khadka, A. M. Saeidi
 J. Liu, D. Batory, and C. Lengauer. Feat
 K. Bennett. Legacy systems: Coping with 
 NASCIO. Digital states at risk modernizi
 A. van Deursen, P. Klint, and C. Verhoef
 S. Adolph, W. Hall, and P. Kruchten. Usi
 B. Wu, D. Lawless, J. Bisbal, R. Richard
 A. Mehta and G. T. Heineman. Evolving le
 A. Alderson and H. Shah. Viewpoints on l
 M. van Sinderen. Challenges and solution
 B. Kitchenham and S. L. Pfleeger. Princi
 K. A. Nasr, H.-G. Gross, and A. van Deur
 B. P. Lientz, E. B. Swanson, and G. E. T
 P. Runeson and M. Höst. Guidelines for c

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(36)
EventCity Hyderabad, India
CollectionTitle ICSE 2014
NumberOfPages 12
isbn 9781450327565
