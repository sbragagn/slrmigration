
#Searching for model migration strategies
##Citing
ArticleAuthorRef: (81464668888@ACM;a ZoaArticleReference)12

##Authors
Auhor Name: Williams, James R.
Auhor ID: 81464668888@ACM; 
Auhor Affiliations:  University of York, York, UK; 
Auhor Name: Paige, Richard F.
Auhor ID: 81552982256@ACM; 
Auhor Affiliations:  University of York, York, UK; 
Auhor Name: Polack, Fiona A. C.
Auhor ID: 81100310214@ACM; 
Auhor Affiliations:  University of York, York, UK; 

##Abstract
Metamodels, like many software artefacts, are subject to evolution. If a metamodel evolves, models that previously conformed to the metamodel may become non-conformant, and must be migrated to reestablish conformance. Manually migrating models can be tedious and error prone, and a number of solutions have arisen to aid in the creation of a migration strategy - a model transformation which automates the migration. This paper asks whether we can make use of research in the field of Search-Based Software Engineering to automatically discover migration strategies, and discusses the challenges involved. In particular, we explore how tools that provide coupled evolution of metamodels and models provide a suitable platform for which to apply search techniques.
##Keywords

##Overview
The article presents the possibility of tackling down the migration of instances of a model, after a model modification, as a operational research method. The idea is to use evolve an existing model by genetic algorithms and use the new version of a meta model (such as the UML specification) to see if the model complies or not. 

##References
###This article is cited by
 Proceedings of the 12th European Conference on Modelling Foundations and Applications - Volume 9764 (10.1007/978-3-319-42061-5_9)
 Proceedings of the ACM/IEEE 42nd International Conference on Software Engineering (10.1145/3377811.3380324)
 A survey on search-based model-driven engineering (10.1007/s10515-017-0215-4)
 2014 IEEE Eighth International Conference on Research Challenges in Information Science (RCIS). (http://ieeexplore.ieee.org/document/6861074/)
 Detecting complex changes and refactorings during (Meta)model evolution (10.1016/j.is.2016.05.002)
 Proceedings of the ACM/IEEE 42nd International Conference on Software Engineering: New Ideas and Emerging Results (10.1145/3377816.3381727)
 Approaches to Co-Evolution of Metamodels and Models (10.1109/TSE.2016.2610424)
 AHM: Handling Heterogeneous Models Matching and Consistency via MDE (http://link.springer.com/10.1007/978-3-030-22559-9_13)
 Towards Minimizing the Impact of Changes Using Search-Based Approach (http://link.springer.com/10.1007/978-3-319-99241-9_14)
###This article Cites
 B. Baudry, S. Ghosh, et al. Barriers to 
 M. Herrmannsdoerfer, S. D. Vermolen, and
 J. R. Williams, R. F. Paige, D. S. Kolov
 K. Garcés, F. Jouault, P. Cointe, and J.
 S. Maoz, J. O. Ringert, and B. Rumpe. A 
 L. M. Rose, D. S. Kolovos, R. F. Paige, 
 R. Poli, W. B. Langdon, and N. F. McPhee
 M. Harman and B. F. Jones. Search based 
 J. Clark, J. J. Dolado, M. Harman, R. Hi
 M. Herrmannsdoerfer, D. Ratiu, and G. Wa
 S. Maoz, J. Ringert, and B. Rumpe. CDDif
 M. Kessentini, H. Sahraoui, M. Boukadoum
 . Lulu, 2009. Available for free at http
 M. Herrmannsdoerfer and M. Koegel. Towar
 . Addison-Wesley Longman Publishing Co.,
 Proceedings of the Joint Workshop on Mod
 . Bradford Company, Scituate, MA, USA, 2
 L. M. Rose, M. Herrmannsdoerfer, J. R. W
 . Springer Berlin Heidelberg New York, s
 M. Herrmannsdoerfer, S. Benz, and E. Jue

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(39)
EventCity Innsbruck, Austria
CollectionTitle ME '12
NumberOfPages 6
isbn 9781450317986
