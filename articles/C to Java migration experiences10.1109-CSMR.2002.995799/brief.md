
#C to Java migration experiences
##Citing
ArticleAuthorRef: (37438656200@IEEE;a ZoaArticleReference)2

##Authors
Auhor Name: Martin, J.
Auhor ID: 37438656200@IEEE; 
Auhor Affiliations:  
Auhor Name: Muller, H.A.
Auhor ID: 37271730100@IEEE; 
Auhor Affiliations:  

##Abstract
With the growing popularity of the Java programming language for both client and server side applications in network-centric computing, there is a rising need for programming libraries that can be easily integrated into Java programs. In a previous paper, we surveyed current strategies for integrating C source code into Java programs, pointed out their weaknesses and presented goals for an improved migration approach. In this paper, we present the Ephedra approach to software migration and report on the results of three case studies transliterating C source code to Java using the Ephedra environment.
##Keywords
C to Java migration;Application software;Computer applications;client-server systems;Computer languages;Computer science;Web and internet services;C language;Ephedra approach;network-centric computing;Java;programming libraries;Network servers;Internet;software migration;Information systems;Web server;C source code integration;object-oriented programming;Computer networks;software libraries;C source code transliteration

##References
###This article is cited by
 Marco Trudel, Carlo A. Furia, Martin Nordio, Bertrand Meyer, Manuel Oriol, "C to O-O Translation: Beyond the Easy Stuff", (https://scholar.google.com/
 Antonio Menezes Leitao, "Migration of Common Lisp Programs to the Java Platform -The Linj Approach", (https://scholar.google.com/scholar?as_q=Migratio
 W.E. Wong, J.J. Li, "Redesigning legacy systems into the object-oriented paradigm", (https://scholar.google.com/scholar?as_q=Redesigning+legacy+system
 Thiago Tonelli Bartolomei, Krzysztof Czarnecki, Ralf Lämmel, "Swing to SWT and back: Patterns for API migration by wrapping", (https://scholar.google.
 Harry M. Sneed, "Migrating from COBOL to Java", (https://scholar.google.com/scholar?as_q=Migrating+from+COBOL+to+Java&as_occt=title&hl=en&as_sdt=0%2C3
 J. Martin, J. Gutenberg, "Automated source code transformations on fourth generation languages", (https://scholar.google.com/scholar?as_q=Automated+so
 Ruediger Gad, Martin Kappes, Inmaculada Medina-Bulo, "Local programming language barriers in stream-based systems", (https://scholar.google.com/schola
###This article Cites
 J. Martin. Ephedra: A C to Java Migratio
 A. A. Terekhov and C. Verhoef. The Reali
 sgraph Standalone Programs. http://www. 
 K. Kontogiannis, J. Martin, K. Wong, R. 
 A. Cimitile, A. D. Lucia, G. A. Di Lucca
 J. George and B. D. Carter. A strategy f
 M.-A. D. Storey, K. Wong, P. Fong, D. Ho
 C. Laffra. C2J, a C++ to Java translator
 H. Erdogmus and O. Tanir, editors. Advan
 J. Gosling, B. Joy, and G. Steele. The J
 E. Mattes. emx 0. 9d (GCC and tools for 
 M.-A. Storey, K. Wong, and H. A. Müller.
 T. Waddington. Java Backend for GCC. htt
 A. S. Yeh, D. R. Harris, and H. B. Ruben
 H. A. Müller and K. Klashinsky. Rigi-A s
 E. D. Demaine. Converting C Pointers to 
 Novosoft. C2J-C to Java translator. http
 American National Standard X3. 159-1989.
 B. W. Kernighan and D. M. Ritchie. The C
 P. E. Livadas and T. Johnson. A new appr
 S. Liang. The Java Native Interface, Pro
 M. Himsolt. Sgraph programmer's manual, 
 U. of Victoria. RigiWeb Server. http://w
 S. Liu and N. Wilde. Identifying objects
 K. Yasumatsu and N. Doi. SPiCE: A System
 J. Martin and H. A. Müller. Discovering 
 F. Lindholm and F. Yellin. The Java Virt
 I. Tilevich. Translating C++ to Java. Fi
 H. Gall and R. Klösch. Finding objects i
 G. Canfora, A. Cimitile, and M. Munro. A
 A. A. Terekhov. Automating Language Conv
 T. Wen. Translating C++ to Java: Resolut
 A. J. Malton. The Migration Barbell. Fir
 J. Martin and H. A. Müller. Strategies f

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=7817
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'995799' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 13-13 March 2002
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ))
pdfUrl nil
isNumber 21484
isPromo false
startPage 143
isEphemera false
isACM false
isSAE false
mlTime PT0.057028S
isMorganClaypool false
sourcePdf 00995799.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/7817/21484/00995799.pdf
articleId 995799
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 2002
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1534-5351' ))
mediaPath /mediastore_new/IEEE/content/media/7817/21484/995799
endPage 153
isStaticHtml true
confLoc Budapest, Hungary, Hungary
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/995799/
accessionNumber 7254447
html_flag true
lastupdate 2020-10-17
pubLink /xpl/conhome/7817/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2002
htmlAbstractLink /document/995799/
issueLink /xpl/tocresult.jsp?isnumber=21484
displayPublicationTitle Proceedings of the Sixth European Conference on Software Maintenance and Reengineering
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 7817
dbTime 3 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 2002
isProduct false
metrics a Dictionary('citationCountPaper'->12 'citationCountPatent'->1 'totalDownloads'->165 )
citationCount 12
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle C to Java migration experiences
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=C+to+Java+migration+experiences&isbn=0-7695-1438-3&publicationDate=2002&author=J.+Martin&ContentID=10.1109/CSMR.2002.995799&orderBeanReset=true&startPage=143&endPage=153&proceedingName=Proceedings+of+the+Sixth+European+Conference+on+Software+Maintenance+and+Reengineering
publicationTitle Proceedings of the Sixth European Conference on Software Maintenance and Reengineering
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-7695-1438-3' ))
xplore-pub-id 7817
xplore-issue 21484
isConference true
displayDocTitle C to Java migration experiences
doiLink https://doi.org/10.1109/CSMR.2002.995799
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 995799
