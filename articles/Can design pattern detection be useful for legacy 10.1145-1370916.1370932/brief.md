
#Can design pattern detection be useful for legacy systemmigration towards SOA?
##Citing
ArticleAuthorRef: (81100275727@ACM;a ZoaArticleReference)8

##Authors
Auhor Name: Arcelli, Francesca
Auhor ID: 81100275727@ACM; 
Auhor Affiliations:  Università degli Studi di Milano-Bicocca, Milano, Italy; 
Auhor Name: Tosi, Christian
Auhor ID: 81351609174@ACM; 
Auhor Affiliations:  Università degli Studi di Milano-Bicocca, Milano, Italy; 
Auhor Name: Zanoni, Marco
Auhor ID: 81479657536@ACM; 
Auhor Affiliations:  Università degli Studi di Milano-Bicocca, Milano, Italy; 

##Abstract
Legacy systems maintenance involves different decisions, often very complex and sometimes requiring high costs and time. Hence studying and applying the right system modernization technique becomes very important for systems evolution. One of the solutions often adopted to modernize a system is the possibility to migrate it towards a SOA architecture. A lot of works in the literature have been done in this direction, which propose methodologies that provide some kind of migration strategy.In the migration process one of the main tasks is related to system comprehension. We often have to analyze not well documented systems, where it is difficult to identify the components which could become services or to recognize the possible problems we could face during the migration process. Software architecture reconstruction is certainly a relevant key activity, which is used for these purposes.In this paper we explore if design pattern detection could be also useful in the migration process: knowing that some design patterns have been applied in the system could give relevant hints to take decisions during migration.
##Keywords
design pattern detection;soa
##Overview
The article starts by recognicing that different classical patterns from the literature can have their place in service oriented architectures.It then proposes that the recognition of such patterns can be useful in the context of migration to SOA architectures.The article gives a list of patterns from the gang of four with their possible usage on SOA migration. __ Patterns that can be used as service candidate__* Façade 	- Intent: Provide an unified interface* Mediator 	- Intent: Defines the interaction between objects* Singleton 	- Intent: Ensure single instance and global access point* Bridge  	- Intent: Decouple abstraction from implementation* Adapter	- Intent: Map the interface of a class to the an expected interface.* Decotrator	- Intent: Attach additional responsibilities to an object dynamically. * Proxy	- Intent: Provide a placeholder for another object to control-access to it.* Command	- Intent: Encapsulate request as an object.__Others__* Abstract Factory	- Intent: Provide an interface for creating families of objects	- Usage: Recognice high level abstractions and entry points.* Chain of responsibility	- Intent: Avoid coupling between request and handler. 	- Usage: Recognice possible service orchestration* State	- Intent: Encapsulate state and transitions of state	- Usage: SOA does not work fine with state. Useful to recognice where state is managed... * Observer	- Intent: Defines one to many dependancy	- Usage: Relate dependancies between services. 

##References
###This article is cited by
 http://services.igi-global.com/resolvedoi/resolve.aspx?doi=10.4018/978-1-4666-4301-7.ch036 (10.4018/978-1-4666-4301-7.ch036)
 http://link.springer.com/10.1007/978-3-642-17694-4_13 (http://link.springer.com/10.1007/978-3-642-17694-4_13)
 A systematic literature review on SOA migration (10.1002/smr.1712)
 http://services.igi-global.com/resolvedoi/resolve.aspx?doi=10.4018/978-1-4666-0155-0.ch002 (10.4018/978-1-4666-0155-0.ch002)
 Translating a Legacy Stack to Microservices Using a Modernization Facade with Performance Optimization for Container Deployments (http://link.springer
 Opening web applications for third-party development (10.1007/s11761-016-0192-7)
###This article Cites
 K. Sartipi and K. Kontogiannis. Managing
 P. Newcomb. Architecture-driven moderniz
 E. Gamma, R. Helm, R. Johnson, and J. Vl
 D. Pollet, S. Ducasse, L. Poyet, I. Allo
 F. Arcelli and C. Raibulet. Program comp
 V. Huston. Design patterns. Web site. ht
 G. Lewis, E. Morris, D. Smith, and L. O?
 F. Arcelli, C. Raibulet, Y.-G. Guéhéneuc
 K. Kontogiannis, G. A. Lewis, D. B. Smit
 M. Lanza. Object oriented Reverse Engine
 F. Arcelli, C. Tosi, M. Zanoni, S. Maggi
 Codelogic. Web site. http://www.logicexp
 Code migration planning and assessment w
 Doxygen. Web site. http://www.stack.nl/~
 Sa4j: Structural analysis for java. Web 
 C. Stoermer, L. O?Brien, and C. Verhoef.
 Soa: Oo and so - how you gonna get there
 K. Bierhoff, M. Grechanik, and E. S. Lio
 J. Snell. Web services programming tips 
 Swag kit. Web site. http://www.swag.uwat
 Software evolution and reverse engineeri
 L. O?Brien, D. Smith, and G. Lewis. Supp

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(63)
EventCity Leipzig, Germany
CollectionTitle SDSOA '08
NumberOfPages 6
isbn 9781605580296
