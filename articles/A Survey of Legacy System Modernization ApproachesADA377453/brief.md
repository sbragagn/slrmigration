
#A Survey of Legacy System Modernization Approaches
##Citing
ArticleAuthorRef: (Comella-Dorda, Santiago @MIL;a ZoaArticleReference)0

##Authors
Auhor Name: Comella-Dorda, Santiago 
Auhor ID: Comella-Dorda, Santiago @MIL; 
Auhor Affiliations:  
Auhor Name: Wallnau, Kurt
Auhor ID: Wallnau, Kurt@MIL; 
Auhor Affiliations:  
Auhor Name: Seacord, Robert C. 
Auhor ID: Seacord, Robert C. @MIL; 
Auhor Affiliations:  
Auhor Name: Robert, John
Auhor ID: Robert, John@MIL; 
Auhor Affiliations:  

##Abstract
Information systems are critical assets for modern enterprises and incorporate key knowledge acquired over the life of an organization. Although these systems must be updated continuously to reflect evolving business practices, repeated modification has a cumulative effect on system complexity, and the rapid evolution of technology quickly renders existing technologies obsolete. Eventually, the existing information systems become too fragile to modify and too important to discard. However, organizations must consider modernizing these legacy systems to remain viable. The commercial market provides a variety of solutions to this increasingly common problem of legacy system modernization. However, understanding the strengths and weaknesses of each modernization technique is paramount to select the correct solution and the overall success of a modernization effort. This paper provides a survey of modernization techniques including screen scraping, database gateway, XML integration, database replication, CGI integration, object-oriented wrapping, and componentization of legacy systems. This general overview enables engineers performing legacy system modernization to preselect a subset of applicable modernization techniques for further evaluation.
##Keywords
client server systems;software engineering;hypertext;integrated systems;data bases;computer gateways;object oriented programming;graphic user interfaces
##Overview
##OverviewThe article puts on context the software evolution and it recognice three main different levels of evolution. Evolution happens specially to successful software, and succeessful software oftenly become critical. ###Maintenance	A maintenance task is oftenly related with minor enhancements, bug fixing and minor features.	It does not consider the adoption of new technologies. It's cost increases with time. Normally the successive small modifications erodes the original design and architecture. 	###Replacement	Replacement tasks are epic tasks that respond to the full reimplementation of the software. This kind method is oftenly named cold turkey. It may be appropiated to some highly specific cases. but most of the times it turns into an impossible task with a worthless outcome. 	Replacement tasks require intensive testing. A full new development disregard all the tunning and robustness of the legacy original system, even thou is not possible to guarantee an outcome with such stability and tunning  in a convenient amount of time. 	###Modernization	Modernization involves extensive changes, including technolocal adoptions, but it keeps as much as possible of the existing logic. 	There are two main branches of modernization according to the level of abstraction of the used technique. 	__Whitebox__	Requires a deep understanding of the mechanics of the legacy system. Is it oftenly related with reverse engineering techniques for instrumenting analysis tools for better understanding of a large legacy system.	The techniques included in this branch are  different ways of code restructuration (such as refactors) related with some specific architectural variable such as maintenability, performance, flexibility.__Blackbox__	The main dish of this paper (all the following survey is on this scope). 	Oftenly based on wrapping techniques. It consist on analysing inputs / outputs of the system, wrapping these artefacts to be able to use them differently.								
##Blackbox: UI Modernization
##Screen scraping	This technique is based mainly interpreting an existing UI.	An HTML (by example) screen is instrumented to iterface the user. The backend will interact with an interactive terminal program.		This is a quick-to-implement technique. It is based on the existing software, it does not modify nor add any functional feature. 	From the point of view of the legacy system, nothing has chaged. However this technique does not enhance the quality of the legacy software in any way. 	## Other usage	Other way to use the same idea is to generate an API from the user interfaces. 
##Data Modernization
The data may need to change the way to be accessed and / or organized.###Database Gateways	A gateway offers a standar API based on a specific accessing model (Such as SQL), to access different standars or adhoc implementations.* Open Database Connectivity. (ODBC) Microsoft. Bridge* Java Database Connectivity (JDBC). Sun - Java. Bridge* Object Data Management Group (ODMG). OMG. Guidelines and standards to guarantee simplicity and portability. ###XML Integration	Many applications on Business 2 Business do exchange data by XML standard structured data. 	Normally a system (legacy or not) is able to connect though internet by http protocol and exchange data in this format. 	This allows different systems to interoperate and exchange data in a standar way. ###Database replication	This technique is about replicating the data of a legacy database in some modern technology. This replications are synchronized periodically with the legacy system by the execution of batch processes. 	By doing this we allow different ways to access and reduce the requirement to access constantly remote data.	
##Functional Modernization
The modernition of functional artefacts (functions, classes etc),  in our Blackbox context, implies the wrapping of both the logic and the data. ###CGI Integration	Common Gatewy Interface is a standard for interfacing web servers with an external application. 	This method allows to have newly developped GUI connecting to specific features in the legacy system, without being restricted to only the existing GUI(like it happens with screen scraping technique). 	However this technique does not address the quality and maintenance  problems of the software. ###Object Oriented Wrapping	This technique is about using objects to applications, services and data, and allowing to communicate and orchestrate them from a higher abstraction level. Nevertheless this approach may be deceptively simple, since it requires a high understanding of the wrapped entity. 	The main technology remarked is the OMG CORBA specification ###Component Wrapping	Similar to OO Wrapping, this technique adapts the wrappee to a component oriented architecture. Components are most likely to be compatible with stream of data what makes them interesting for interconnecting existing legacy systems. 	The main technologies available are- Distributed interNet Architecture (DNA) from Microsoft - CORBA3 Component Model from OMG- Enterprise Java Beans from Sun.

##References
###This article is cited by
###This article Cites

##Article Metadata
