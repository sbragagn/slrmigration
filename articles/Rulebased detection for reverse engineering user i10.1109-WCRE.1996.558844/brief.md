
#Rule-based detection for reverse engineering user interfaces
##Citing
ArticleAuthorRef: (37270392400@IEEE;a ZoaArticleReference)996

##Authors
Auhor Name: Moore, M.M.
Auhor ID: 37270392400@IEEE; 
Auhor Affiliations:  

##Abstract
Reengineering the user interface can be a critical part of the migration of any large information system. The paper details experiences with manually reverse engineering legacy applications to build a model of the user interface functionality, and to develop a technique for partially automating this process. The results show that a language-independent set of rules can be used to detect user interface components from legacy code, and also illustrate problems that require dynamic analysis to solve them.
##Keywords
legacy code;Information systems;Workstations;user interface reverse engineering;user interface functionality model;manual reverse engineering;user interface component detection;Automation;dynamic analysis;reverse engineering;partial automation;Open systems;large information system migration;Business process re-engineering;Computer interfaces;legacy applications;Educational institutions;user interface reengineering;User interfaces;Reverse engineering;language-independent rules;rule-based detection;Graphical user interfaces
##Overview
The article proposes the case of migration from terminal interface to GUI. In this migration the means of interaction are not fully clear and distinct.Recogniced UI Functional Component- Error Message	A simple one way communication to the user. Recogniced by the usage of error output- User Input	Recogniced because of reading the STDInput.- Output user Message	One way communication. non interaction is required.- Command selection	The user is prompted to choose one command- Continuos selection	The user action iterates over a compound object - User action function	Action performed as result of user interaction- User dialog	a two way communication . - Precondition	condition indicates that predicate must be true in or ther for an action to be invoked- Postcondition	invoked after an action to affect a precondition variable	Reverse engineering 	Isolating the user interface subset	After getting a call tree from a feature, identify all the calls to IO primitives.	Categorize by kind of usage: input/output 	Some rules on the code can help us to get us to map different uses to our differnet UI concepts.		Like that the usage of switch case is related to command selection, what can be mapped to a menu.	The paper has a validation!!!!!!	

##References
###This article is cited by
 K. Tucker, R.E.K. Stirewalt, "Model based user-interface reengineering", (https://scholar.google.com/scholar?as_q=Model+based+user-interface+reenginee
 G. Abowd, A. Goel, D.F. Jerding, M. McCracken, M. Moore, J.W. Murdock, C. Potts, S. Rugaber, L. Wills, "MORALE. Mission ORiented Architectural Legacy 
 L. Bouillon, Q. Limbourg, J. Vanderdonckt, B. Michotte, "Reverse engineering of Web pages based on derivations and transformations", (https://scholar.
 M.M. Moore, S. Rugaber, "Domain analysis for transformational reuse", (https://scholar.google.com/scholar?as_q=Domain+analysis+for+transformational+re
 Moon-Soo Lee, Seok-Gyoo Shin, Young-Jong Yang, "The design and implementation of Enterprise JavaBean (EJB) wrapper for legacy system", (https://schola
 A. Memon, I. Banerjee, A. Nagarajan, "GUI ripping: reverse engineering of graphical user interfaces for testing", (https://scholar.google.com/scholar?
 C. Potts, "ScenIC: a strategy for inquiry-driven requirements determination", (https://scholar.google.com/scholar?as_q=ScenIC%3A+a+strategy+for+inquir
 M. Moore, S. Rugaber, "Using knowledge representation to understand interactive systems", (https://scholar.google.com/scholar?as_q=Using+knowledge+rep
 M.M. Moore, L. Moshkina, "Migrating legacy user interfaces to the Internet: shifting dialogue initiative", (https://scholar.google.com/scholar?as_q=Mi
 A. Panteleymonov, "Interoperable thin client separation from GUI applications", (https://scholar.google.com/scholar?as_q=Interoperable+thin+client+sep
 S. Rugaber, "A tool suite for evolving legacy software", (https://scholar.google.com/scholar?as_q=A+tool+suite+for+evolving+legacy+software&as_occt=ti
 J. Vanderdonckt, L. Bouillon, N. Souchon, "Flexible reverse engineering of web pages with VAQUISTA", (https://scholar.google.com/scholar?as_q=Flexible
 André M. P. Grilo, Ana C. R. Paiva, João Pascoal Faria, "Reverse engineering of GUI models for testing", (https://scholar.google.com/scholar?as_q=Reve
###This article Cites
 J. Foley, W. C. Kim, S. Kovacevic and K.
 E. Merlo, J. F. Girard, K. Kontogiannis,
 H. Ritsch and H. M. Sneed, "Reverse Engi
 P. Sukaviriya, M. Frank, A. Spaans, T. G
 E. Merlo, P.-Y. Gagne, J.-F. Girard, K. 
 K. Kamper and S. Rugaber, A Reverse Engi
 A. Dix, J. Finlay, G. Abowd and R. Beale
 J. Sutton and R. Sprague, "A Survey of B
 M. Green, "The University of Alberta Use
 Y. Jayachandra, Re-Engineering the Netwo

##Article Metadata
isPromo false
pdfPath /iel3/4138/12181/00558844.pdf
isMarketingOptIn false
doiLink https://doi.org/10.1109/WCRE.1996.558844
isProduct false
isFreeDocument false
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'false' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
contentType conferences
accessionNumber 5444429
rightsLinkFlag 1
content_type Conferences
citationCount 17
metrics a Dictionary('citationCountPaper'->17 'citationCountPatent'->1 'totalDownloads'->98 )
isEphemera false
isBook false
mlTime PT0.019891S
chronDate 8-10 Nov 196
openAccessFlag F
isCustomDenial false
allowComments false
htmlAbstractLink /document/558844/
endPage 48
isStaticHtml false
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-8186-7674-4' ))
isGetArticle false
isChapter false
isMorganClaypool false
pdfUrl nil
isJournal false
isConference true
dbTime 2 ms
pubLink /xpl/conhome/4138/proceeding
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ) a Dictionary('name'->'General Topics for Engineers' ))
publicationNumber 4138
isNotDynamicOrStatic true
articleNumber 558844
isEarlyAccess false
issueLink /xpl/tocresult.jsp?isnumber=12181
ml_html_flag false
publicationYear 1996
isOUP false
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=Rule-based+detection+for+reverse+engineering+user+interfaces&isbn=0-8186-7674-4&publicationDate=1996&author=M.M.+Moore&ContentID=10.1109/WCRE.1996.558844&orderBeanReset=true&startPage=42&endPage=48&proceedingName=Proceedings+of+WCRE+%2796%3A+4rd+Working+Conference+on+Reverse+Engineering
publicationTitle Proceedings of WCRE '96: 4rd Working Conference on Reverse Engineering
sourcePdf 00558844.pdf
isGetAddressInfoCaptured false
xploreDocumentType Conference Publication
isSMPTE false
articleId 558844
isOpenAccess false
lastupdate 2020-08-03
isNow false
html_flag false
displayPublicationTitle Proceedings of WCRE '96: 4rd Working Conference on Reverse Engineering
conferenceDate 11-10 Nov. 1996
isBookWithoutChapters false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'558844' 'type'->'PDF' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
chronOrPublicationDate 8-10 Nov 196
confLoc Monterey, CA, USA, USA
isACM false
isStandard false
startPage 42
displayDocTitle Rule-based detection for reverse engineering user interfaces
getProgramTermsAccepted false
ephemeraFlag false
isNumber 12181
isDynamicHtml false
formulaStrippedArticleTitle Rule-based detection for reverse engineering user interfaces
isSAE false
mediaPath 
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=4138
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
