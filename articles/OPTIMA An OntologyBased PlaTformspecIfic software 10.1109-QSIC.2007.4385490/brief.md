
#OPTIMA: An Ontology-Based PlaTform-specIfic software Migration Approach
##Citing
ArticleAuthorRef: (37292152900@IEEE;a ZoaArticleReference)7

##Authors
Auhor Name: Zhou, Hong
Auhor ID: 37292152900@IEEE; 
Auhor Affiliations:  
Auhor Name: Kang, Jian
Auhor ID: 37288719300@IEEE; 
Auhor Affiliations:  
Auhor Name: Chen, Feng
Auhor ID: 37278821100@IEEE; 
Auhor Affiliations:  
Auhor Name: Yang, H.
Auhor ID: 37278138100@IEEE; 
Auhor Affiliations:  

##Abstract
Software migration is an inherently knowledge intensive activity, which requires a great amount of expertise and experience from different sources. Knowledge-based approach to software migration has a great potential for improving traditional approaches. In this paper, an Ontology-based PlaTform-specific software Migration Approach (OPTIMA) is proposed. Firstly, the background and the related work are introduced, and the ontology design for platform- specific software migration is discussed. Secondly, the architecture and workflow of OPTIMA are illustrated with a prototype tool. Thirdly, the case study is used for experiments with the approach and the prototype tool, and finally, the conclusion is drawn and further research directions are speculated.
##Keywords
Knowledge representation;model driven architecture;Application software;ontology design;Computer architecture;Software prototyping;knowledge based systems;Logic;Prototypes;Software tools;Database languages;OPTIMA;Ontologies;ontologies (artificial intelligence);Software design;knowledge intensive activity;Knowledge-based approach;ontology-based platform-specific software migration approach;software architecture
##Overview
On the context of software migration the author proposes to use ontological based transformation for fitting parameters from one domain to other. 
Like this, when migrating an application from one platform API to a new one, we can simply repeat the same knowledge. 
The paper works on a  C embedded program migrating thread libraries. 

This tool does not propose any validation. The Evaluation proposed shows the results on migrations of 30 C and C Header files, where 14 have thread API call. 
According to the numbers only 1 NONPOSIX Thread call where migrated by hand out of 38. The rest was handled automaically.	


##References
###This article is cited by
 Hong Zhou, Feng Chen, Hongji Yang, "Developing Application Specific Ontology for Program Comprehension by Combining Domain Ontology with Code Ontology
 Hong Zhou, "COSS: Comprehension by ontologising software system", (https://scholar.google.com/scholar?as_q=COSS%3A+Comprehension+by+ontologising+softw
 Jian Kang, Jianjun Pu, Jianchu Huang, Zihou Zhou, Hongji Yang, "Business Intelligence Recovery from Legacy Code", (https://scholar.google.com/scholar?
 Feng Chen, Hong Zhou, Hongji Yang, Martin Ward, William Cheng-Chung Chu, "Requirements Recovery by Matching Domain Ontology and Program Ontology", (ht
 Han Li, Feng Chen, Hongji Yang, He Guo, William Cheng-Chung Chu, Yuansheng Yang, "An Ontology-Based Approach for GUI Testing", (https://scholar.google
 Hong Zhou, Hongji Yang, Andrew Hugill, "An Ontology-Based Approach to Reengineering Enterprise Software for Cloud Computing", (https://scholar.google.
 Feng Chen, Zhuopeng Zhang, Jianzhi Li, Jian Kang, Hongji Yang, "Service Identification via Ontology Mapping", (https://scholar.google.com/scholar?as_q
###This article Cites
 H. Yang, X. Liu and H. Zedan, "Tackling 
 D. Jin and J. R. Cordy, "Ontology-Based 
 H. Andrade and J. Saltz, "Towards a Know
 O. Corcho, M. Fernandez-Lopez and A. Gom
 H. Yang, Z. Cui and P. O'Brien, "Extract
 "Protege Ontology Editor and Knowledge A
 "Protege Programming Development Kit (PD
 W. L. Johnson and E. Soloway, "PROUST: K
 M. K. Smith, C. Welty and D. McGuinness,
 V. Devedzic, "Understanding Ontological 
 Y. Li, H. Yang and W. Chu, "A Concept-Or
 W. Heuvel, "Matching and Adaptation: Cor
 I. Horrocks, P. F. Patel-Schneider, H. B
 F. Chen, H. Yang, H. Guo et al., "Aspect
 H. Yang and M. Ward, "Successful Evoluti
 Y. Zhang, R. Witte, J. Rilling et al., "
 "MDA Guide Version 1.0.1", (https://scho
 F. Chen, H. Guo, L. Dai et al., "An Appl
 "DIG Interface", (https://scholar.google
 K. Miriyala and M. T. Harandi, "Automati
 F. Baader, D. Calvanese, D. McGuinness e
 D. Djuric, V. Devedzic and D. Gasevic, "
 D. Sidarkeviciute, E. Tyugu and A. Kuusi
 K. L. Mills and H. Gomaa, "Knowledge-Bas
 F. Chen, S. Li and H. Yang, "Feature Ana
 D. Fensel, "Ontologies: Silver Bullet fo
 2006-an OrderedCollection(37271164700@IE
 T. Eisenbarth, R. Koschke and D. Simon, 
 W. Johnson, M. S. Feather and D. R. Harr
 E. Prud'hommeaux and A. Seaborne, "SPARQ

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=4385458
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'4385490' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 11-12 Oct. 2007
pubTopics an OrderedCollection(a Dictionary('name'->'Computing and Processing' ) a Dictionary('name'->'Communication, Networking and Broadcast Technologies' ))
pdfUrl nil
isNumber 4385459
isPromo false
startPage 143
isEphemera false
isACM false
isSAE false
mlTime PT0.059893S
isMorganClaypool false
sourcePdf 30350143.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel5/4385458/4385459/04385490.pdf
articleId 4385490
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 11-12 Oct. 2007
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print ISSN' 'value'->'1550-6002' ) a Dictionary('format'->'Electronic ISSN' 'value'->'2332-662X' ))
mediaPath /mediastore_new/IEEE/content/media/4385458/4385459/4385490
endPage 152
isStaticHtml true
confLoc Portland, OR, USA
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/4385490/
accessionNumber 9879897
html_flag true
lastupdate 2020-07-13
pubLink /xpl/conhome/4385458/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2007
htmlAbstractLink /document/4385490/
issueLink /xpl/tocresult.jsp?isnumber=4385459
displayPublicationTitle Seventh International Conference on Quality Software (QSIC 2007)
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'true' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 4385458
dbTime 2 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 11-12 Oct. 2007
isProduct false
metrics a Dictionary('citationCountPaper'->9 'citationCountPatent'->0 'totalDownloads'->141 )
citationCount 9
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle OPTIMA: An Ontology-Based PlaTform-specIfic software Migration Approach
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=OPTIMA%3A+An+Ontology-Based+PlaTform-specIfic+software+Migration+Approach&isbn=978-0-7695-3035-2&publicationDate=Oct.+2007&author=Hong+Zhou&ContentID=10.1109/QSIC.2007.4385490&orderBeanReset=true&startPage=143&endPage=152&proceedingName=Seventh+International+Conference+on+Quality+Software+%28QSIC+2007%29
publicationTitle Seventh International Conference on Quality Software (QSIC 2007)
isbn an OrderedCollection(a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'0-7695-3035-4' ) a Dictionary('format'->'Print ISBN' 'isbnType'->'' 'value'->'978-0-7695-3035-2' ))
xplore-pub-id 4385458
xplore-issue 4385459
isConference true
displayDocTitle OPTIMA: An Ontology-Based PlaTform-specIfic software Migration Approach
doiLink https://doi.org/10.1109/QSIC.2007.4385490
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 4385490
