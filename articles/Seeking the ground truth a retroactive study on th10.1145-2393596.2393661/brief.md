
#Seeking the ground truth: a retroactive study on the evolution and migration of software libraries
##Citing
ArticleAuthorRef: (81470655677@ACM;a ZoaArticleReference)12

##Authors
Auhor Name: Cossette, Bradley E.
Auhor ID: 81470655677@ACM; 
Auhor Affiliations:  University of Calgary, Calgary, Alberta, Canada; 
Auhor Name: Walker, Robert J.
Auhor ID: 81350587870@ACM; 
Auhor Affiliations:  University of Calgary, Calgary, Alberta, Canada; 

##Abstract
Application programming interfaces (APIs) are a common and industrially-relevant means for third-party software developers to reuse external functionality. Several techniques have been proposed to help migrate client code between library versions with incompatible APIs, but it is not clear how well these perform in an absolute sense. We present a retroactive study into the presence and nature of API incompatibilities between several versions of a set of Java-based software libraries; for each, we perform a detailed, manual analysis to determine what the correct adaptations are to migrate from the older to the newer version. In addition, we investigate whether any of a set of adaptation recommender techniques is capable of identifying the correct adaptations for library migration. We find that a given API incompatibility can typically be addressed by only one or two recommender techniques, but sometimes none serve. Furthermore, those techniques give correct recommendations, on average, in only about 20% of cases.
##Keywords
API;adaptive change;recommendation systems
##Overview
The article proposes the study of a corpus of API Change. The study reveals different changes in the API of a library from one version to the next one in order to classify them according to the kind of effort needed to do  in order to adapt the existing usage of the API to the new API interface.The general taxonomy is split as followFully Automatable	- pull up		- remove parameter	- reorganize parametersPartially Automatable 	- Shorten subtype string 		using a supertype 	- Encapsulate		change the way the functionality was encapsulated	- Expose implementation		removal of convenience methods (requires to rewrite using a more abstract api)	- Consolidate 		Classes methdos and / or fields are replaced by generalized versions		Hard to Automate	- Replace by External API		functionality was removed and replace by an other library	- Add contextual data		requires more information than before	- Change type, supertype or remap 		fields or method change the typing somehow. 	-Add to interface		In the case of extention, an interface or superclass to extend from library adds new methods.	-Extract to configuration 		Entities are removed to be supplied by configuration	-Immutable		An entity becomes immutable	-Hide 		reduction of the visibility (public to protected/private)	- Delete		The functionality was fully removed. No replacement available	-Repair deliberately damaged 			A bug in the new version not yet repaired. 							

##References
###This article is cited by
 Proceedings of the 15th International Conference on Mining Software Repositories (10.1145/3196398.3196420)
 Workshop on open and original problems in software language engineering (http://ieeexplore.ieee.org/document/6671334/)
 Proceedings of the 26th International Conference on Industrial, Engineering and Other Applications of Applied Intelligent Systems (10.1007/978-3-642-3
 Proceedings of the 2014 IEEE 14th International Working Conference on Source Code Analysis and Manipulation (10.1109/SCAM.2014.30)
 Proceedings of the 2016 24th ACM SIGSOFT International Symposium on Foundations of Software Engineering (10.1145/2950290.2950306)
 Proceedings of the 28th ACM Joint Meeting on European Software Engineering Conference and Symposium on the Foundations of Software Engineering (10.114
 http://link.springer.com/10.1007/978-3-030-00262-6_6 (http://link.springer.com/10.1007/978-3-030-00262-6_6)
 Proceedings of the 2016 24th ACM SIGSOFT International Symposium on Foundations of Software Engineering (10.1145/2950290.2950325)
 Technical Obsolescence Management Strategies for Safety-Related Software for Airborne Systems (http://link.springer.com/10.1007/978-3-319-74730-9_34)
 Proceedings of the 2015 Workshop on Partial Evaluation and Program Manipulation (10.1145/2678015.2682534)
 On the reaction to deprecation of clients of 4 + 1 popular Java APIs and the JDK (10.1007/s10664-017-9554-9)
 https://linkinghub.elsevier.com/retrieve/pii/S0164121214001198 (https://linkinghub.elsevier.com/retrieve/pii/S0164121214001198)
 How do developers react to API evolution? A large-scale empirical study (10.1007/s11219-016-9344-4)
 Proceedings of the 2018 26th ACM Joint Meeting on European Software Engineering Conference and Symposium on the Foundations of Software Engineering (1
 Can automated pull requests encourage software developers to upgrade out-of-date dependencies? (10.5555/3155562.3155577)
 Proceedings of the 2016 24th ACM SIGSOFT International Symposium on Foundations of Software Engineering (10.1145/2950290.2950298)
 2013 20th Working Conference on Reverse Engineering (WCRE). (http://ieeexplore.ieee.org/document/6671295/)
 Proceedings of the 41st International Conference on Software Engineering (10.1109/ICSE.2019.00117)
 Journal of Software: Evolution and Process (10.1002/smr.1660)
 Automatic discovery of function mappings between similar libraries (http://ieeexplore.ieee.org/document/6671294/)
 On the use of information retrieval to automate the detection of third-party Java library migration at the method level (10.1109/ICPC.2019.00053)
 2014 Software Evolution Week - IEEE Conference on Software Maintenance, Reengineering and Reverse Engineering (CSMR-WCRE). (http://ieeexplore.ieee.org
 MigrationMiner: An Automated Detection Tool of Third-Party Java Library Migration at the Method Level (https://ieeexplore.ieee.org/document/8919240/)
 Proceedings of the 13th International Conference on Mining Software Repositories (10.1145/2901739.2901762)
 https://doi.org/10.1016/j.infsof.2015.02.014 (10.1016/j.infsof.2015.02.014)
 Do developers update their library dependencies? (10.1007/s10664-017-9521-5)
 https://ieeexplore.ieee.org/document/8186224/ (https://ieeexplore.ieee.org/document/8186224/)
 Proceedings of the 2013 IEEE 20th International Conference on Web Services (10.1109/ICWS.2013.48)
 Can automated pull requests encourage software developers to upgrade out-of-date dependencies? (http://ieeexplore.ieee.org/document/8115621/)
 2013 10th IEEE Working Conference on Mining Software Repositories (MSR 2013). (http://ieeexplore.ieee.org/document/6624037/)
 Proceedings of the 2013 International Workshop on Ecosystem Architectures (10.1145/2501585.2501589)
 Proceedings of the 2015 IEEE International Conference on Software Maintenance and Evolution (ICSME) (10.1109/ICSM.2015.7332471)
 Proceedings of the 2013 IEEE International Conference on Software Maintenance (10.1109/ICSM.2013.18)
 Proceedings of the 27th International Conference on Program Comprehension (10.1109/ICPC.2019.00052)
 2015 IEEE 22nd International Conference on Software Analysis, Evolution and Reengineering (SANER). (http://ieeexplore.ieee.org/document/7081869/)
 Proceedings of the 2015 30th IEEE/ACM International Conference on Automated Software Engineering Workshop (ASEW) (10.1109/ASEW.2015.21)
 An exploratory study of api changes and usages based on apache and eclipse ecosystems (10.1007/s10664-015-9411-7)
 API Usage Change Rules Mining based on Fine-grained Call Dependency Analysis (10.1145/3131704.3131707)
 Proceedings of the 15th International Conference on Mining Software Repositories (10.1145/3196398.3196416)
 Proceedings of the 4th International Workshop on Recommendation Systems for Software Engineering (10.1145/2593822.2593827)
 Journal of Software: Evolution and Process (http://doi.wiley.com/10.1002/smr.1893)
 Software&mdash;Practice &amp; Experience (10.1002/spe.2484)
 2015 IEEE/ACM 12th Working Conference on Mining Software Repositories (MSR). (http://ieeexplore.ieee.org/document/7180082/)
 Proceedings of the 10th Working Conference on Mining Software Repositories (10.5555/2487085.2487136)
 2016 IEEE International Conference on Software Maintenance and Evolution (ICSME). (http://ieeexplore.ieee.org/document/7816485/)
 Proceedings of the 12th Working Conference on Mining Software Repositories (10.5555/2820518.2820546)
 What Java developers know about compatibility, and why this matters (10.1007/s10664-015-9389-1)
 Mining Unit Tests for Discovery and Migration of Math APIs (10.1145/2629506)
 Proceedings of the 40th International Conference on Software Engineering (10.1145/3180155.3180170)
 http://link.springer.com/10.1007/978-3-642-45135-5_12 (http://link.springer.com/10.1007/978-3-642-45135-5_12)
 Proceedings of the 16th International Conference on Mining Software Repositories (10.1109/MSR.2019.00061)
 An empirical study on the impact of refactoring activities on evolving client-used APIs (10.1016/j.infsof.2017.09.007)
 2018 IEEE International Conference on Software Maintenance and Evolution (ICSME). (https://ieeexplore.ieee.org/document/8530083/)
 Proceedings of the 10th International Workshop on Context-Oriented Programming: Advanced Modularity for Run-time Composition (10.1145/3242921.3242923)
###This article Cites
 M. Nita and D. Notkin. Using twinning to
 D. Dig, C. Comertoglu, D. Marinov, and R
 B. Cossette and R. Walker. Polylingual d
 P. Weissgerber and S. Diehl. Identifying
 J. Henkel and A. Diwan. CatchUp!: Captur
 R. Johnson. Frameworks = (components + p
 Refactoring: Improving the Design of Exi
 W. Wu, Y.-G. Guéhéneuc, G. Antoniol, and
 Companion ACM SIGPLAN Conf. Obj.-Oriente
 I. Balaban, F. Tip, and R. Fuhrer. Refac
 P. Kapur, B. Cossette, and R. Walker. Re
 B. Dagenais and M. Robillard. Recommendi
 C. Larman. Protected variation: The impo
 M. Robillard and R. DeLine. A field stud
 B. Morel and P. Alexander. SPARTACAS: Au
 . Java SE 7 edition, 2012. http://docs.o
 Proc. ACM/IEEE Int. Conf. Softw. Eng (10
 J. Perkins. Automatically generating ref
 M. Godfrey and L. Zou. Using origin anal
 D. Dig, S. Negara, V. Mohindra, and R. J
 Z. Xing and E. Stroulia. API-evolution s
 D. Dig and R. Johnson. How do APIs evolv
 P. Kapur. Refactoring references for lib
 K. Chow and D. Notkin. Semi-automatic up
 T. Schäfer, J. Jonas, and M. Mezini. Min
 M. Kim, D. Notkin, and D. Grossman. Auto
 M. Lehman. Programs, life cycles, and la

##Article Metadata
NumberOfPages 11
pages an OrderedCollection(1)
EventType PAPER_CONFERENCE
CollectionTitle FSE '12
CollectionNumber 55
PublisherCity New York, NY, USA
Number 55
isbn 9781450316149
EventCity Cary, North Carolina
