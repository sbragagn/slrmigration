
#Reverse engineering strategies for software migration (tutorial)
##Citing
ArticleAuthorRef: (81335494840@ACM;a ZoaArticleReference)997

##Authors
Auhor Name: Müller, Hausi A.
Auhor ID: 81335494840@ACM; 
Auhor Affiliations:  Department of Computer Science, University of Victoria, Victoria, B.C., V8W 3P6 Canada; 

##Abstract
ABSTRACT No abstract available.
##Keywords
software evolution;reverse engineering;software migration;software reengineering;legacy systems
##Overview
The article talks about the general challenges on software evolution remarking the need of research on reengineering, migration and reverse engeneering strategies.It points out how expensive is to tackle down the  Y2K problem. 

##References
###This article is cited by
 Reverse Business Engineering — Modelle aus produktiven R/3-Systemen ableiten (http://link.springer.com/10.1007/978-3-642-58663-7_23)
###This article Cites
 A. Cimitile and H. Muller (eds). Procs, 
 S. Bohner and A. Cimitile (eds). Interna
 L. Wills, I. Baxter, and E. Chikofsky (e
 M. Brodie and M. Stonebraker. Migrating 
 R. Arnold. Software Reengineering. IEEE 
 I. Graham. Migrating to Object Technolog

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(659)
EventCity Boston, Massachusetts, USA
CollectionTitle ICSE '97
NumberOfPages 2
isbn 0897919149
