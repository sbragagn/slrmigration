
#A lean and mean strategy for migration to services
##Citing
ArticleAuthorRef: (81435604886@ACM;a ZoaArticleReference)12

##Authors
Auhor Name: Razavian, Maryam
Auhor ID: 81435604886@ACM; 
Auhor Affiliations:  VU University Amsterdam, The Netherlands; 
Auhor Name: Lago, Patricia
Auhor ID: 81100511826@ACM; 
Auhor Affiliations:  VU University Amsterdam, The Netherlands; 

##Abstract
In industry, enterprises have many software systems to be modernized and made available as added-value services. Identification of migration strategies and practices for service engineering is critical for successful legacy migration, and SOA adoption in industrial setting. This paper presents the results of an interview survey on the migration strategies in industry. The purpose of this paper is twofold: 1) to discover the migration strategies that industrial practice adopts 2) to identify the benefits of making such strategies explicit. Results of the survey have been analyzed in terms of migration activities and the available knowledge assets. As a result, we generalize the practice of industrial migration into a Lean &amp; Mean SOA migration strategy. In addition, the uses of the strategy pinpoint promising industry-relevant research directions.
##Keywords
interview survey;migration to services;activity;knowledge
##Overview
This paper presents the results of many interviews on how does the industry tackles the problem of migration. The article exposes that the enterprise participants, regardless their origin, tackle down the problem with similar activities and knowledge. And unlikely to the academic approaches, SOA migration in industry neglects the reverse engineering. The main research questions proposed by the article are: __RQ1: What activities are carried out?____RQ2: What are the knowledge elements that are used and produced?__###RQ1__Understanding Legacy system and Target system__To gain the required understanding of the legacy systm non reverse engineering is used. They use the "stake holders minds". Reverse engineering is consider to be expensive since it requires specific tools development. __Gap Analysis__Done to undetstand the gap to save to arrive to target system. Normally obtained by alligning business objectives and IT investment. __Forward engineering__	Analysis, design and implementation of software services. After defining the migration target properties . __Tranforming Legacy assets to services__	Mostly by wrapping	###RQ2__Knowledge to create migration lifecycle plan__	Business goals, risks, costs/investment and constraints. __Knowledge to create ideal services__	__Knowleddge to create software services__ ###How Core Activities Benefit Practice 	- Costs and risks of activities is of main importance	- Practices related to different activities (reuse previous decisions)	###How Core Knowledge Benefits practice 	- Knowledge is in people's minds and documents	- Validity period for knowledge elements: explicit expiration time, or availability (if the person that knows is not available, is a problem)	- What can change: underline knowledge that may change. 			 

##References
###This article is cited by
 A systematic literature review on SOA migration (10.1002/smr.1712)
 Journal of Software: Evolution and Process (10.1002/smr.1613)
 Sustaining Runtime Performance while Incrementally Modernizing Transactional Monolithic Software towards Microservices (10.1145/2851553.2892039)
 2013 IEEE 7th International Symposium on the Maintenance and Evolution of Service-Oriented and Cloud-Based Systems (MESOCA). (http://ieeexplore.ieee.o
 Perspectives and reflections on cloud computing and internet technologies from NordiCloud 2012 (10.1145/2513534.2513547)
 Proceedings of the 2015 IEEE International Conference on Software Maintenance and Evolution (ICSME) (10.1109/ICSM.2015.7332497)
###This article Cites
 J. F. Meyer. Service Oriented Architectu
 G. Lewis, E. Morris, and D. Smith. Analy
 2009-an OrderedCollection(81435604886@AC
 M. Razavian and P. Lago. A Frame of Refe
 R. Kazman, S. G. Woods, and S. J. Carriè
 Case Study Research, Design and Methods 
 Gartner. Hype Cycle for Application Deve
 Research design: qualitative, quantitati
 The knowledge-creating company: How japa
 Interviewing As Qualitative Research: A 
 2nd workshop on Software engineering for
 Qualitative Data Analysis: An Expanded S
 Migrating legacy systems: gateways, inte
 Joint Conference on Software Architectur
 ICSE 2011 Panel on What Industry Wants f
 E. R. Poort and H. van Vliet. Architecti
 N. Brown, Y. Cai, Y. Guo, R. Kazman, M. 
 S. R. Tilley and D. Smith. Perspectives 
 M. Razavian and P. Lago. A Survey of SOA
 H. M. Sneed. Integrating legacy Software

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(61)
EventCity Helsinki, Finland
CollectionTitle WICSA/ECSA '12
NumberOfPages 8
isbn 9781450315685
