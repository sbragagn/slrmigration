
#Code migration through transformations: an experience report
##Citing
ArticleAuthorRef: (81100496122@ACM;a ZoaArticleReference)998

##Authors
Auhor Name: Kontogiannis, K.
Auhor ID: 81100496122@ACM; 
Auhor Affiliations:  University of Waterloo, Dept. of Electrical Eng.; 
Auhor Name: Martin, J.
Auhor ID: 81100430616@ACM; 
Auhor Affiliations:  University of Victoria, Dept. of Computer Science; 
Auhor Name: Wong, K.
Auhor ID: 81332536356@ACM; 
Auhor Affiliations:  University of Victoria, Dept. of Computer Science; 
Auhor Name: Gregory, R.
Auhor ID: 81406592414@ACM; 
Auhor Affiliations:  University of Toronto, Dept. of Computer Science; 
Auhor Name: Müller, Hausi A.
Auhor ID: 81335494840@ACM; 
Auhor Affiliations:  University of Victoria, Dept. of Computer Science; 
Auhor Name: Mylopoulos, J.
Auhor ID: 81100183754@ACM; 
Auhor Affiliations:  University of Toronto, Dept. of Computer Science; 

##Abstract
One approach to dealing with spiraling maintenance costs, manpower shortages and frequent breakdowns for legacy code is to "migrate" the code into a new platform and/or programming language. The objective of this paper is to explore the feasibility of semiautomating such a migration process in the presence of performance and other constraints for the migrant code. In particular, the paper reports on an experiment involving a medium-size software system written in PL/IX. Several modules of the system were migrated to C++, first by hand and then through a semi-automatic tool. After discovering that the migrant code was performing up to 50% slower than the original, a second migration effort was conducted which improved the performance of the migrant code substantially. The paper reports on the transformation techniques used by the transformation process and the effectiveness of the prototype tools that were developed. In addition, the paper presents preliminary results on the evaluation of the experiment.
##Keywords

##Overview
#Overview

##References
###This article is cited by
 Proceedings of the 21st annual ACM SIGPLAN conference on Object-oriented programming systems, languages, and applications (10.1145/1167473.1167481)
 The varying faces of a program transformation systems (10.1145/2077808.2077825)
 Proceedings of the IEEE International Conference on Software Maintenance (ICSM'01) (10.1109/ICSM.2001.972782)
 Towards lightweight checks for mass maintenance transformations (10.1016/j.scico.2005.01.001)
 Proceedings of the Conference on The Future of Software Engineering (10.1145/336512.336526)
 https://doi.org/10.1145/1167515.1167481 (10.1145/1167515.1167481)
 Proceedings of the 1999 conference of the Centre for Advanced Studies on Collaborative research (10.5555/781995.782003)
 Towards improving interface modularity in legacy Java software through automated refactoring (10.1145/2892664.2892681)
 Addendum to the 2000 proceedings of the conference on Object-oriented programming, systems, languages, and applications (Addendum) (10.1145/367845.368
 Refactoring support for class library migration (10.1145/1103845.1094832)
 Large-scale, AST-based API-usage analysis of open-source Java projects (10.1145/1982185.1982471)
 Proceedings of the 12th Innovations on Software Engineering Conference (formerly known as India Software Engineering Conference) (10.1145/3299771.3299
 Proceedings of the 20th annual ACM SIGPLAN conference on Object-oriented programming, systems, languages, and applications (10.1145/1094811.1094832)
###This article Cites
 {5} Brodie, M., Stonebraker, M., "Migrat
 {10} Gillespie, D., "A Pascal To C Conve
 {18} Yasumatsu, K., Doi, N., "Spice: A S
 {1} Baker S. B, "Parameterized Pattern M
 {4} van den Brand, M., Sellink, A., Verh
 {8} Feldman, S., Gay, D., Maimone, M., S
 {15} Sigma Research Inc. : http://www.si
 {7} Consortium for Software Engineering 
 {6} Chen, Y., Nishimoto, M., Ramamoorthy
 {2} Ballance, R., Graham, S., Van De Van
 {3} Borras, P., Clement, D., Despeyroux,
 {17} Xinotech Inc. http://www.xinotech.c
 {9} Finnigan, P. et.al "The Software Boo
 {12} Ladd, D., Ramming, J., "A*: a Langu
 {13} Paul, S., Prakash, A., "A Framework
 {11} Johnson, H., "Substring Matching fo
 {16} Steffen, J., "Interactive examinati
 {14} Reps, T., Teitelbaum, T., "The Synt

##Article Metadata
EventType PAPER_CONFERENCE
pages an OrderedCollection(13)
EventCity Toronto, Ontario, Canada
CollectionTitle CASCON '98
NumberOfPages 13
