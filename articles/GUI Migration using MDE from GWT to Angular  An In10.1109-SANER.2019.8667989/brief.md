
#GUI Migration using MDE from GWT to Angular 6: An Industrial Case
##Citing
ArticleAuthorRef: (37086799870@IEEE;a ZoaArticleReference)19

##Authors
Auhor Name: Verhaeghe, Benoît
Auhor ID: 37086799870@IEEE; 
Auhor Affiliations:  
Auhor Name: Etien, Anne
Auhor ID: 37564515300@IEEE; 
Auhor Affiliations:  
Auhor Name: Anquetil, Nicolas
Auhor ID: 37443066400@IEEE; 
Auhor Affiliations:  
Auhor Name: Seriai, Abderrahmane
Auhor ID: 37085402324@IEEE; 
Auhor Affiliations:  
Auhor Name: Deruelle, Laurent
Auhor ID: 37541602600@IEEE; 
Auhor Affiliations:  
Auhor Name: Ducasse, Stéphane
Auhor ID: 37264962400@IEEE; 
Auhor Affiliations:  
Auhor Name: Derras, Mustapha
Auhor ID: 37085546754@IEEE; 
Auhor Affiliations:  

##Abstract
During the evolution of an application, it happens that developers must change the programming language. In the context of a collaboration with Berger-Levrault, a major IT company, we are working on the migration of a GWT application to Angular. We focus on the GUI aspect of this migration which, even if both frameworks are web Graphical User Interface (GUI) frameworks, is made difficult because they use different programming languages and different organization schema. Such migration is complicated by the fact that the new application must be able to mimic closely the visual aspect of the old one so that the users of the application are not disrupted. We propose an approach in four steps that uses a meta-model to represent the GUI at a high abstraction level. We evaluated this approach on an application comprising 470 Java (GWT) classes representing 56 pages. We are able to model all the web pages of the application and 93% of the widgets they contain, and we successfully migrated 26 out of 39 pages (66%). We give examples of the migrated pages, both successful and not.
##Keywords
Visualization;programming languages;Berger-Levrault;GUI aspect;Model-Driven Engineering;GWT;Angular;Internet;GUI migration;graphical user interfaces;Industrial case study;migrated pages;Migration;User Interfaces;Tools;Web pages;Java;industrial case;Web graphical user interface frameworks;MDE;Business;GWT application;programming language;visual aspect;Graphical user interfaces
##Overview
The article proposes a MDE based migration from GWT to Angular.The migration is based on MDE modernization approach. First loads a the visual component structure. After it does  creates a  visual model based on UI components analysis.The proposed components include the structural components and layout.This model is finally used to generate the destination widgets.The paper explains the domain restrictions and requirements. 	Visual constant result. It proposes a manual validation to ensure the meeting of the requirements / restrictions and the visual quality of the product. 

##References
###This article is cited by
###This article Cites
 M. E. Joorabchi and A. Mesbah, "Reverse 
 A. Memon, I. Banerjee and A. Nagarajan, 
 S. Staiger, "Reverse engineering of grap
 Z. Gotti and S. Mbarki, "Java swing mode
 J. Brant, D. Roberts, B. Plendl and J. P
 Ó. Sánchez Ramán, J. Sánchez Cuadrado an
 H. Samir, A. Kamel and E. Stroulia, "Swi
 F. Fleurey, E. Breton, B. Baudry, A. Nic
 V. Lelli, A. Blouin, B. Baudry, F. Coulo
 I. C. Morgado, A. Paiva and J. P. Faria,
 S. Ducasse, N. Anquetil, U. Bhatti, A. C
 O. Nierstrasz, S. Ducasse and T. Gîrba, 
 J. C. Silva, C. C. Silva, R. D. Goncalo,
 K. Garcés, R. Casallas, C. Álvarez, E. S
 E. Shah and E. Tilevich, "Reverse-engine
 Moore, Rugaber and Seaver, "Knowledge-ba

##Article Metadata
persistentLink https://ieeexplore.ieee.org/servlet/opac?punumber=8663526
isNow false
subType IEEE Conference
purchaseOptions a Dictionary('mandatoryBundle'->false 'optionalBundle'->false 'otherPricingInfoAvailable'->false 'pdfPricingInfo'->an OrderedCollection(a Dictionary('memberPrice'->'$14.95' 'nonMemberPrice'->'$33.00' 'partNumber'->'8667989' 'type'->'PDF/HTML' )) 'pdfPricingInfoAvailable'->true 'showOtherFormatPricingTab'->false 'showPdfFormatPricingTab'->true )
conferenceDate 24-27 Feb. 2019
pubTopics an OrderedCollection(a Dictionary('name'->'Components, Circuits, Devices and Systems' ) a Dictionary('name'->'Engineering Profession' ))
pdfUrl nil
isNumber 8667965
isPromo false
startPage 579
isEphemera false
isACM false
isSAE false
mlTime PT0.066755S
isMorganClaypool false
sourcePdf saner19ind-id235-p.pdf
isFreeDocument false
isDynamicHtml true
pdfPath /iel7/8663526/8667965/08667989.pdf
articleId 8667989
isMarketingOptIn false
isOpenAccess false
isSMPTE false
chronOrPublicationDate 24-27 Feb. 2019
isEarlyAccess false
isnn an OrderedCollection(a Dictionary('format'->'Print on Demand(PoD) ISSN' 'value'->'1534-5351' ))
mediaPath /mediastore_new/IEEE/content/media/8663526/8667965/8667989
endPage 583
isStaticHtml false
confLoc Hangzhou, China, China
isGetAddressInfoCaptured false
content_type Conferences
_value IEEE
htmlLink /document/8667989/
accessionNumber 18524894
html_flag false
lastupdate 2020-10-31
pubLink /xpl/conhome/8663526/proceeding
isChapter false
isStandard false
isCustomDenial false
publicationYear 2019
htmlAbstractLink /document/8667989/
issueLink /xpl/tocresult.jsp?isnumber=8667965
displayPublicationTitle 2019 IEEE 26th International Conference on Software Analysis, Evolution and Reengineering (SANER)
isGetArticle false
userInfo a Dictionary('delegatedAdmin'->false 'desktop'->false 'fileCabinetContent'->false 'fileCabinetUser'->false 'guest'->false 'individual'->false 'institute'->false 'institutionalFileCabinetUser'->false 'isCwg'->false 'isDelegatedAdmin'->false 'isInstitutionDashboardEnabled'->false 'isInstitutionProfileEnabled'->false 'isMdl'->false 'isRoamingEnabled'->false 'member'->false 'showGet802Link'->false 'showOpenUrlLink'->false 'showPatentCitations'->true 'subscribedContent'->false 'tracked'->false )
sections a Dictionary('abstract'->'true' 'algorithm'->'false' 'authors'->'true' 'cadmore'->'false' 'citedby'->'false' 'dataset'->'false' 'definitions'->'false' 'disclaimer'->'false' 'figures'->'true' 'footnotes'->'false' 'keywords'->'true' 'metrics'->'true' 'multimedia'->'false' 'references'->'true' )
ml_html_flag true
openAccessFlag F
isJournal false
publicationNumber 8663526
dbTime 2 ms
rightsLinkFlag 1
contentType conferences
isBookWithoutChapters false
xploreDocumentType Conference Publication
chronDate 24-27 Feb. 2019
isProduct false
metrics a Dictionary('citationCountPaper'->0 'citationCountPatent'->0 'totalDownloads'->99 )
ephemeraFlag false
allowComments false
formulaStrippedArticleTitle GUI Migration using MDE from GWT to Angular 6: An Industrial Case
rightsLink http://s100.copyright.com/AppDispatchServlet?publisherName=ieee&publication=proceedings&title=GUI+Migration+using+MDE+from+GWT+to+Angular+6%3A+An+Industrial+Case&isbn=978-1-7281-0591-8&publicationDate=Feb.+2019&author=Beno%C3%AEt+Verhaeghe&ContentID=10.1109/SANER.2019.8667989&orderBeanReset=true&startPage=579&endPage=583&proceedingName=2019+IEEE+26th+International+Conference+on+Software+Analysis%2C+Evolution+and+Reengineering+%28SANER%29
publicationTitle 2019 IEEE 26th International Conference on Software Analysis, Evolution and Reengineering (SANER)
isbn an OrderedCollection(a Dictionary('format'->'Electronic ISBN' 'isbnType'->'' 'value'->'978-1-7281-0591-8' ) a Dictionary('format'->'Print on Demand(PoD) ISBN' 'isbnType'->'' 'value'->'978-1-7281-0592-5' ))
xplore-pub-id 8663526
xplore-issue 8667965
isConference true
displayDocTitle GUI Migration using MDE from GWT to Angular 6: An Industrial Case
doiLink https://doi.org/10.1109/SANER.2019.8667989
isOUP false
isNotDynamicOrStatic false
isBook false
getProgramTermsAccepted false
articleNumber 8667989
