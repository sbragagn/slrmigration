
#Workshop on object-oriented legacy systems and software evolution
##Citing
ArticleAuthorRef: (81100579680@ACM;a ZoaArticleReference)995

##Authors
Auhor Name: Taivalsaari, Antero
Auhor ID: 81100579680@ACM; 
Auhor Affiliations:  Nokia Research Center, P.O. Box 45, 00211 Helsinki, Finland; 
Auhor Name: Trauter, Roland
Auhor ID: 81100079773@ACM; 
Auhor Affiliations:  Daimler-Benz Research Center, Wilhelm-Runge Strasse 11, 89081 Ulm, Germany; 
Auhor Name: Casais, Eduardo
Auhor ID: 81100630914@ACM; 
Auhor Affiliations:  Forschungszentrum Informatik, Haid-und-Neu-Strasse 10-14, 76131 Karlsruhe, Germany; 

##Abstract
ABSTRACT No abstract available.
##Keywords


##References
###This article is cited by
 ROMEO: reverse engineering from OO source code to OMT design (http://ieeexplore.ieee.org/document/723189/)
###This article Cites

##Article Metadata
EventType PAPER_CONFERENCE
PublisherCity New York, NY, USA
pages an OrderedCollection(180)
EventCity Austin, Texas, USA
CollectionTitle OOPSLA '95
NumberOfPages 6
isbn 0897917219
